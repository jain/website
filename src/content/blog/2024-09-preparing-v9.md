---
title: Help us in making Forgejo v9 a great release
publishDate: 2024-09-25
tags: ['contributing']
excerpt: 'Your help in preparing the next Forgejo release is appreciated. Now is a good time to step in.'
---

We are looking forward to shipping the improvements we have made in the past months to all Forgejo users.
Today, we have reached the next milestone in our [release cycle](/docs/v8.0/developer/release/#release-cycle),
reaching a period of feature freeze to focus on fixing bugs.
The release of Forgejo v9.0 is scheduled for 16 October 2024.

It is a good moment for you to help ensuring the next Forgejo release is a success.
Thank you for your support!

## Localization

With the feature freeze comes a period of less new strings to translate.
Now is the best time to translate Forgejo into your language,
we invite you to [check out the localization guide](/docs/next/contributor/localization/)
and joining us on Weblate.

Localizing Forgejo helps more users around the world to get involved in free/libre software development,
including end-users submitting bug reports and students who get in touch with Forgejo in schools.

Additionally, raising the completion status before the next release reduces the effort on our end to backport translations to existing Forgejo releases.

Join our localization effort today!

## Using

If you can upgrade your Forgejo version to the latest state in the [`v9.0/forgejo`](https://codeberg.org/forgejo/forgejo/src/branch/v9.0/forgejo) branch,
you can spot and report issues before other users do.
Ensure you make a backup of your data, just in case.

We are looking forward to your feedback or bug reports.
Test the upcoming version of Forgejo today!

## Developing

If you always wanted to help developing for Forgejo,
we appreciate your help in fixing bugs for the upcoming version.

Take a look at [this list of bugs that might be "good first issue"s](https://codeberg.org/forgejo/forgejo/issues?labels=201023%2c222666).
Let us know if you are interested to take a look at any of them,
and we'll be here to assist you completing them before the release date.

If you can spare more time, also take a look at other and new bug reports,
try to reproduce them and diagnose the issues together with the team.

## Donating

If you cannot spare some time right now, but still want to support Forgejo,
consider [setting up a donation to our Liberapay team](https://liberapay.com/forgejo)
or [donating to Codeberg](https://docs.codeberg.org/improving-codeberg/donate/)
to enable other Forgejo developers to complete their work.

### Get in touch

If you have any feedback or suggestions for Forgejo, we'd love to hear from you! Open an issue on [our issue tracker](https://codeberg.org/forgejo/forgejo/issues) for feature requests or bug reports. You can also find us [on the Fediverse](https://floss.social/@forgejo), or drop by [our Matrix space](https://matrix.to/#/#forgejo:matrix.org) ([main chat room](https://matrix.to/#/#forgejo-chat:matrix.org)) to say hi!
