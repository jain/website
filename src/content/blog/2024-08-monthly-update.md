---
title: Forgejo monthly update - August 2024
publishDate: 2024-08-31
tags: ['news', 'report']
excerpt: Forgejo changed its license from MIT to GNU GPL v3+, it is now copyleft, just like Git. A pull request for federated user activity following using ActivityPub saw significant progress. Space usage quotas for users and organizations was implemented. The Forgejo security policy was published.
---

The monthly report is meant to provide a good overview of what has changed in Forgejo in the past month. If you would like to help, please get in touch in [the chatroom](https://matrix.to/#/#forgejo-chat:matrix.org) or participate in the [ongoing discussions](https://codeberg.org/forgejo/discussions).

It was decided more than a year ago and finally happened: Forgejo changed its license from MIT to [GNU GPL v3+](https://forgejo.org/2024-08-gpl/) and accepts contributions with a simple [Developer Certificate of Origin](https://forgejo.org/docs/next/developer/dco/). It is an additional guarantee that it will not drift away from Free Software and become [Open Core](https://en.wikipedia.org/wiki/Open-core_model) like GitLab or Gitea.

Last month, the [v8.0 release date was postponed multiple times](https://codeberg.org/forgejo/forgejo/issues/4153) because bugs were discovered at the last minute. This rather time consuming effort was rewarded by a smooth upgrade of Codeberg and other instances. The absence of problems allowed Forgejo contributors to focus on features and structural improvements: ActivityPub federation, storage quotas, security policy, and more.

## Forgejo is now copyleft

[The impact of the license change](https://forgejo.org/2024-08-gpl/) has been carefully considered with regard to the variety of usages of Forgejo. Someone might have chosen to avoid copyleft software, for example because it is discouraged in a company. However, Forgejo depends on Git, one of the most successful copyleft software. Both Forgejo and Git must be used together, either as individual binaries or bundled into the [official container images](https://codeberg.org/forgejo-experimental/-/packages/container/forgejo/v9.0-test). The license of Git is [GNU GPL v2](https://git-scm.com/about/free-and-open-source), another version of the same [copyleft license](https://www.gnu.org/licenses/copyleft.html).

The majority of Forgejo's codebase is still MIT-licensed, but it is expected that an increasing number of files will switch to GNU GPL v3+ over time. With the notable exception of the [API swagger file that is and will stay MIT](https://codeberg.org/forgejo/forgejo/pulls/5083) to clarify that the intent of the Forgejo authors is that it is used for interoperability with no restriction. It is not an original work and enforcing copyright on that file would probably be difficult anyway.

The [discussions on how to improve](https://codeberg.org/forgejo/discussions/issues/192) Forgejo's licensing are still very lively and will eventually lead to decisions that will improve its legal protection, in the interest of the general public.

## Federation

[Federation is getting useful](https://codeberg.org/forgejo/discussions/issues/208). There is now more than preliminary background work, and the first exciting things could be tried out by users. The work is not near the goal yet.

Building upon the foundations released with Forgejo v8.0, a pull request for [federated user activity following](https://codeberg.org/forgejo/forgejo/pulls/4767) saw significant progress. The core idea is that any activity (where activity is defined as anything that ends up in the Forgejo user activity) is wrapped in an ap.Note, and sent to followers in the ActivityPub sense. Similarly, the inbox of local users now accepts such Notes. Additionally, there's now a "Feeds" tab on the user profile page, which displays the received notes.

## go-git support is removed from the codebase

Forgejo used to have 2 Git backends: the normal git and [go-git](https://github.com/go-git/go-git) which is a Git implementation in pure Go. This had 2 benefits:

1. You don't need git installed.
2. It is a little bit faster than Git on Windows.

Supporting go-git would mean holding Forgejo back. Every Git Feature that Forgejo wants to use also needs to be implemented in go-git. For example: setting git notes in the Web UI is currently not possible in go-git. In addition go-git may lead to data loss and repository corruption [(one example)](https://github.com/go-git/go-git/issues/878). It is not widely used and does not have extensive testing (see [the latest example of such corruption](https://github.com/go-git/go-git/issues/878)).

For these reasons, [go-git was removed from the codebase](https://codeberg.org/forgejo/forgejo/pulls/4941). It only affects users who built Forgejo manually using `TAGS=gogit`, which no longer has any effect. This removal only happened in the development branch and not in the existing stable Forgejo branches, up to `v8.0/forgejo` included.

## Noteworthy pull requests

- [Space usage quotas for users and organizations](https://codeberg.org/forgejo/forgejo/pulls/4212).
- [A release asset can be a URL instead of a file](https://codeberg.org/forgejo/forgejo/pulls/1445).
- [Add a cron task to cleanup dangling container images with version sha256:\*](https://codeberg.org/forgejo/forgejo/pulls/4698).
- [Remove support for Couchbase as a session provider](https://codeberg.org/forgejo/forgejo/pulls/5090); it instead will now fallback to the file provider. The rationale for removing Couchbase support is that it's not free software, https://www.couchbase.com/blog/couchbase-adopts-bsl-license/, and therefore cannot be tested in Forgejo and neither should be supported.
- [Allow push mirrors to use a SSH key as the authentication method](https://codeberg.org/forgejo/forgejo/pulls/4819) for the mirroring action instead of using user:password authentication. The SSH keypair is created by Forgejo and the destination repository must be configured with the public key to allow for push over SSH.
- [Forgejo Actions logs are compressed by default](https://codeberg.org/forgejo/forgejo/commit/bf7373a2520ae56a1dc00416efa02de9749b63d3). It can be disabled by setting [actions].LOG_COMPRESSION=none.

Read more [in the draft release notes for the upcoming major version](https://codeberg.org/forgejo/forgejo/milestones).

## OCI mirror

Forgejo [maintains a mirror](https://code.forgejo.org/forgejo/oci-mirror/src/commit/5c750a36ad39692206cc04eca85b6a34b5367a31/.forgejo/workflows/mirror.yml) of [container images](https://code.forgejo.org/oci/-/packages) that are commonly used in the CI and the release process. The primary motivation is to not be subject to rate limiting [when using the Docker hub](https://docs.docker.com/docker-hub/download-rate-limit/) as well as saving bandwidth.

There still were two problems that led to a rate limiting incident disrupting the CI during a few hours:

- A number of references to container images were not using the mirror - they were replaced
- The mirror itself was rate limited because it used `skopeo copy` - it was [replaced with `skopeo sync`](https://code.forgejo.org/forgejo/oci-mirror/pulls/4)

## Release notes automation

In addition to the preview shown in each pull request, the [Forgejo milestones](https://codeberg.org/forgejo/forgejo/milestones) for all upcoming releases are [updated daily with the draft release notes](https://codeberg.org/forgejo/forgejo/src/commit/eb25bc9edb5d33621fbebda20475139f42d62ad7/.forgejo/workflows/release-notes-assistant-milestones.yml) compiled from all the pull requests.

## Design and User Interface

Semantic HTML often was a discussion topic, and [a pull request was merged to demonstrate](https://codeberg.org/forgejo/forgejo/pulls/4995) how forms could look like with less classes and less weird divs all over the place. They bring consistency out of the box (you only need to change some CSS properties, no need to keep your templates in sync). It was followed by [a refactor of some forms to improve semantic HTML, usability, accessibility, and reduce the JavaScript footprint](https://codeberg.org/forgejo/forgejo/pulls/5031).

[A discussion started to improve the testing infrastructure](https://codeberg.org/forgejo/discussions/issues/212). The "reasonable effort" for the tests is eaten up by just figuring out how to get test data populated. Contributors asked to write tests, should not follow a paper chase. It led to pull requests to [move `CreateDeclarativeRepo` to more accessible location](https://codeberg.org/forgejo/forgejo/pulls/5108) and [improve diffs generated by Forgejo to make testing more convenient](https://codeberg.org/forgejo/forgejo/pulls/5110).

## Helm chart

The Forgejo helm chart had [many minor and patch updates](https://code.forgejo.org/forgejo-helm/forgejo-helm/releases), in both v7 and v8. [Helm chart v7.0.5](https://code.forgejo.org/forgejo-helm/forgejo-helm/releases/tag/v7.0.5) and [v8.1.1](https://code.forgejo.org/forgejo-helm/forgejo-helm/releases/tag/v8.1.1) were released which contain Forgejo security fixes.

Each version is tested against a kubernetes cluster to verify it works. It was using [kind](https://kind.sigs.k8s.io/) but it turned out to be difficult to debug when the number of transient errors increased. [K3S](https://k3s.io/) is [used](https://code.forgejo.org/forgejo-helm/forgejo-helm/pulls/773/files) instead and proved to be more stable.

## Forgejo v8.0 install party

[The Forgejo v8.0 install party](https://codeberg.org/forgejo/discussions/issues/198) was a nice community meetup and we got to know some Forgejo users. Some users performed their updates live and had only minor issues that were mostly caused by an issue on their end. Due to the lack of actual problems, some might have perceived it as boring. Finally, it was decided to also upgrade Codeberg to Forgejo v8, which was also a smooth experience.

## Social account

https://floss.social/@forgejo was setup about two years ago and it works flawlessly. However, a problem emerged over the past six months that [requires finding a new home](https://codeberg.org/forgejo/discussions/issues/205): the moderation team at floss.social cannot be contacted, [despite numerous attempts over a period of months and via multiple channels](https://codeberg.org/forgejo/discussions/issues/87).

Nothing indicates it is anything more than a case of being overwhelmed by requests on a rather large instance. But it is best addressed by looking for a new home now instead of waiting that an event requiring moderation happens and is left unattended.

## Security policy

A [discussion began](https://codeberg.org/forgejo/governance/issues/159) in 2023, before Forgejo became a hard-fork of Gitea, to improve the security collaboration with upstream projects. It led to [a security policy](https://pad.gusted.xyz/s/uN6kscBuh) that was [agreed on](https://codeberg.org/forgejo/governance/issues/159) according to the [Forgejo decision making process](https://codeberg.org/forgejo/governance/issues/159).

## Dependency management

A dedicated [renovate repository](https://code.forgejo.org/forgejo-contrib/forgejo-renovate/src/branch/main/.forgejo/workflows/renovate.yml#L21) runs every 30 minutes in the https://code.forgejo.org instance to service Forgejo related projects, saving them the burden of running it individually.

The configuration of renovate within Forgejo spaces is the same with regard to Go dependencies. Instead of repeating them in each repository ([forgejo/renovate.json](https://codeberg.org/forgejo/forgejo/src/commit/d34d8ec2cfd92451edbadb371cd101fdf2160fad/renovate.json), [runner/renovate.json](https://code.forgejo.org/forgejo/runner/src/commit/1008f44ddbfdc732a41b466985cac0785924af18/renovate.json)), they [import a shared configuration](https://code.forgejo.org/forgejo/runner/src/commit/82523d1d8e52f607bf7dd87d64f892b704803354/renovate.json) found in [a repository](https://code.forgejo.org/forgejo/renovate-config/src/commit/9f969e5d320ebad4816c51ae30ff4131dc559802/renovate.json) created for that purpose.

## Localization

5 batches of translation updates were merged with 2090 new strings and 1020 string improvements - more than the previous two months combined.

The localization team keeps making sure that the merged translation updates are backported to the current stable versions of Forgejo, so that the releases are always shipped with the most complete and highest quality translations available.

Forgejo is used by a wide variety of people and organizations around the world. For some of them the availability and quality of translations are important factors. Everyone is welcome to contribute to the localization by translating and checking strings. Details on how to participate can be found [here](https://forgejo.org/docs/next/contributor/localization/).

## Forgejo runner

A new version of the Forgejo runner was published which [fixes a security issue](https://code.forgejo.org/forgejo/runner/src/branch/main/RELEASE-NOTES.md#3-5-1). It was made easier by using the same tooling as Forgejo itself to [upgrade the dependencies](https://code.forgejo.org/forgejo/runner/pulls?poster=163).

Security is the most important aspect that the Forgejo runner needs to address before it can be considered for beta testing and will be helped [by a security audit](https://codeberg.org/forgejo/discussions/issues/204) which is in the early stages with [Radically Open Security](https://www.radicallyopensecurity.com).

It will also need more contributors to help with its long term maintenance and anyone interested is encouraged to join.

## Sustainability

Donations to the [Forgejo Liberapay team](https://liberapay.com/forgejo) reached around 40€ per week and are distributed to [three beneficiaries](https://codeberg.org/forgejo/governance/src/branch/main/TEAMS.md#liberapay-team-members).

Drawing upon [previous sustainability discussions](https://codeberg.org/forgejo/discussions/issues/144), a [grant application was submitted](https://codeberg.org/forgejo/sustainability/pulls?labels=244292) for the [Sovereign Tech Fund](https://www.sovereigntechfund.de/).

The creation of a sustainability team, tasked to map out and implement a strategy on how to make Forgejo a durable endeavour over the next years [was proposed](https://codeberg.org/forgejo/governance/issues/163).

## We Forge

Forgejo is a **community of people** who contribute in an inclusive environment. We forge on an equal footing, by reporting a bug, voicing an idea in the chatroom or implementing a new feature. The following list of contributors is meant to reflect this diversity and acknowledge all contributions since the last monthly report was published. If you are missing, please [ask for an update](https://codeberg.org/forgejo/website/issues/new).

- https://codeberg.org/0ko
- https://codeberg.org/abueide
- https://codeberg.org/AdamGreenberg
- https://codeberg.org/alex19srv
- https://codeberg.org/algernon
- https://codeberg.org/AliveDevil
- https://codeberg.org/Andre601
- https://codeberg.org/arija
- https://codeberg.org/AverageHelper
- https://codeberg.org/avobs
- https://codeberg.org/ayakael
- https://codeberg.org/behm
- https://codeberg.org/bengrue
- https://codeberg.org/Beowulf
- https://codeberg.org/bookworm
- https://codeberg.org/bramh
- https://codeberg.org/caesar
- https://codeberg.org/catgirll
- https://codeberg.org/CPU_Blanc
- https://codeberg.org/dachary
- https://codeberg.org/depeo
- https://codeberg.org/Dirk
- https://codeberg.org/dploeger
- https://codeberg.org/dragon
- https://codeberg.org/earl-warren
- https://codeberg.org/el0n
- https://codeberg.org/ell1e
- https://codeberg.org/eloy
- https://codeberg.org/emilylange
- https://codeberg.org/fadedave
- https://codeberg.org/fnetX
- https://codeberg.org/fontenot
- https://codeberg.org/gedw99
- https://codeberg.org/Gusted
- https://codeberg.org/h759bkyo4
- https://codeberg.org/heartshake
- https://codeberg.org/hexa
- https://codeberg.org/intelfx
- https://codeberg.org/io7m
- https://codeberg.org/ironmagma
- https://codeberg.org/izzy
- https://codeberg.org/JakobDev
- https://codeberg.org/j-dominguez9
- https://codeberg.org/jean-daricade
- https://codeberg.org/jerger
- https://codeberg.org/jthvai
- https://codeberg.org/justinsimmons
- https://codeberg.org/jwildeboer
- https://codeberg.org/KaKi87
- https://codeberg.org/kita
- https://codeberg.org/kuolemaa
- https://codeberg.org/Kwonunn
- https://codeberg.org/l_austenfeld
- https://codeberg.org/liberodark
- https://codeberg.org/LordMZTE
- https://codeberg.org/mahlzahn
- https://codeberg.org/Mai-Lapyst
- https://codeberg.org/maltejur
- https://codeberg.org/marcellmars
- https://codeberg.org/martinwguy
- https://codeberg.org/matrss
- https://codeberg.org/maxadamo
- https://codeberg.org/mehrad
- https://codeberg.org/mickenordin
- https://codeberg.org/mkobel
- https://codeberg.org/mrwsl
- https://codeberg.org/msrd0
- https://codeberg.org/Musselman
- https://codeberg.org/n0toose
- https://codeberg.org/naipotato
- https://codeberg.org/omenos
- https://codeberg.org/paspflue
- https://codeberg.org/patdyn
- https://codeberg.org/paulvt
- https://codeberg.org/Porkepix
- https://codeberg.org/realaravinth
- https://codeberg.org/recursive_recursion
- https://codeberg.org/reynir
- https://codeberg.org/rohitsharma09
- https://codeberg.org/Ryuno-Ki
- https://codeberg.org/schelmo
- https://codeberg.org/sclu1034
- https://codeberg.org/SIMULATAN
- https://codeberg.org/skobkin
- https://codeberg.org/slingamn
- https://codeberg.org/sneakers-the-rat
- https://codeberg.org/snematoda
- https://codeberg.org/solomonv
- https://codeberg.org/stb
- https://codeberg.org/strk
- https://codeberg.org/taifoss
- https://codeberg.org/Techwizz
- https://codeberg.org/tepozoa
- https://codeberg.org/thefox
- https://codeberg.org/thilinajayanath
- https://codeberg.org/toolforger
- https://codeberg.org/viceice
- https://codeberg.org/vwbusguy
- https://codeberg.org/wangyan
- https://codeberg.org/waseigo
- https://codeberg.org/WhyNotHugo
- https://codeberg.org/xlii
- https://codeberg.org/xyhhx
- https://codeberg.org/yarikoptic
- https://codeberg.org/yoctozepto
- https://translate.codeberg.org/user/ciampix
- https://translate.codeberg.org/user/emansije
- https://translate.codeberg.org/user/ewm
- https://translate.codeberg.org/user/Fjuro
- https://translate.codeberg.org/user/hahahahacker2009
- https://translate.codeberg.org/user/hankskyjames777
- https://translate.codeberg.org/user/hoovad
- https://translate.codeberg.org/user/hugoalh
- https://translate.codeberg.org/user/leana8959
- https://translate.codeberg.org/user/lotigara
- https://translate.codeberg.org/user/Outbreak2096
- https://translate.codeberg.org/user/pswsm
- https://translate.codeberg.org/user/qui
- https://translate.codeberg.org/user/Wuzzy
- https://translate.codeberg.org/user/Xinayder
- https://translate.codeberg.org/user/zub
- https://translate.codeberg.org/user/Zughy

A **minority of Forgejo contributors earn a living** by implementing the roadmap co-created by the Forgejo community, see [the sustainability repository](https://codeberg.org/forgejo/sustainability) for the details.
