---
title: Forgejo monthly update - September 2024
publishDate: 2024-10-06
tags: ['news', 'report']
excerpt: Forgejo v9.0 release candidates are available for testing to prepare for the release scheduled 16 October 2024. The contributor and testing documentation were improved with the goal of encouraging more diverse participation. The infrastructure dedicated to Forgejo development suffered a downtime because of excessive crawling and mitigation measures were implemented.
---

The monthly report is meant to provide a good overview of what has changed in Forgejo in the past month. If you would like to help, please get in touch in [the chatroom](https://matrix.to/#/#forgejo-chat:matrix.org) or participate in the [ongoing discussions](https://codeberg.org/forgejo/discussions).

## Forgejo v9.0 release candidates

The first release candidates for [Forgejo v9.0](https://forgejo.org/docs/next/contributor/release/#release-cycle) was published [25 September](https://codeberg.org/forgejo/forgejo/issues/5380) and code.forgejo.org upgraded. A call [for participation](https://forgejo.org/2024-09-preparing-v9/) was published, asking for help with translations and testing.

A [regression was discovered](https://codeberg.org/forgejo/discussions/issues/227) in the v9.0 release candidate that incorrectly deleted some of the images [mirrored from the Docker hub](https://code.forgejo.org/oci/-/packages). It disrupted the CI intermittently during 48h before a [fix was published and deployed](https://codeberg.org/forgejo/forgejo/pulls/5430). The root cause was a bug in the [cron task that cleanup dangling container images](https://codeberg.org/forgejo/forgejo/pulls/4698).

## Forgejo Design process

Efforts on long-term improvements to Forgejo have been kept low in the past month, however there was a noteworthy exchange on [moderation features](https://codeberg.org/forgejo/design/issues/20) in Forgejo. Initially, an idea was investigated to track reported content in a specific issue tracker, but the idea was discarded some time later due to the volume of spam issues on Codeberg, which probably requires a more efficient UI.
The moderation features will not make it into Forgejo v9, but they will remain in the focus of design work.

## Contributor documentation and test suite

By the end of August, a [discussion emerged to improve Forgejo's testing infrastructure](https://codeberg.org/forgejo/discussions/issues/212), making it more friendly to new developers.

Since the last monthly update, multiple improvements have been made:

- The contributing resources in the documentation have been [cleaned and updated](https://codeberg.org/forgejo/docs/pulls/821), with the goal of encouraging more diverse contributions, [sending a warm welcome](https://forgejo.org/docs/next/contributor/welcome/) to new contributors and clarifying the motivation and instructions for writing tests. If you did not yet contribute to Forgejo, now is a good time to get started and provide us with feedback.
- The [in-repo hints for testing](https://codeberg.org/forgejo/forgejo/pulls/5235) have been deduplicated and updated to make getting started easier. And numerous smaller improvements have been made to the end-to-end test suite that uses real browsers to ensure actions in the Forgejo UI work as expected. They [added](https://codeberg.org/forgejo/forgejo/pulls/5322) and [improved](https://codeberg.org/forgejo/forgejo/pulls/5287) examples.

Using the improved test infrastructure, the [frequency of new browser tests](https://codeberg.org/forgejo/forgejo/commits/branch/forgejo/tests/e2e) has increased a lot compared to recent months.

## Helm chart

A new major version, [9.0.0](https://code.forgejo.org/forgejo-helm/forgejo-helm/releases/tag/v9.0.0) was published. It sets proper namespaces and allows override.

The Forgejo helm chart had [many minor and patch updates](https://code.forgejo.org/forgejo-helm/forgejo-helm/releases), in both v7 and v8. [Helm chart v7.1.2](https://code.forgejo.org/forgejo-helm/forgejo-helm/releases/tag/v7.1.2) and [v8.2.3](https://code.forgejo.org/forgejo-helm/forgejo-helm/releases/tag/v8.1.1) are the latest.

## Localization

A significant effort was made to backport translations to the LTS version (Forgejo v7.0). There was hope for new languages added in this version to reach better completion in its primary lifetime, but it is taking longer than expected. This and other considerations led to [a proposal](https://codeberg.org/forgejo/discussions/issues/226) to do less backporting to old stable to prevent breakage and save time.

A very large change was [ported from Gitea](https://codeberg.org/forgejo/forgejo/pulls/5306) and is good for internationalization. But it was not easy to also preserve the existing strings and it could have broken a few non-English strings in Forgejo v9. Some translators caught the problem and it was luckily fixed in time.

The overall translation activity was about twice lower than last month, which was very active.

## Forgejo Actions

The [security audit](https://code.forgejo.org/forgejo/nlnet-off-ngie-forgejo) bootstrapped [last month](/2024-08-monthly-update/#forgejo-runner) has its own repository to track the work done as transparently as possible. A [suitable pentester](https://code.forgejo.org/forgejo/nlnet-off-ngie-forgejo/issues/1) was found and [the scope of the audit](https://code.forgejo.org/forgejo/nlnet-off-ngie-forgejo/issues/2) was determined during a call. A rough plan was [drafted](https://code.forgejo.org/forgejo/nlnet-off-ngie-forgejo/issues/3) and approved by NLnet who is funding the audit. The work should begin in November 2024.

## Optimizing CI pipelines

In order to ensure a high software quality, Forgejo (like most larger project) runs CI/CD pipelines that perform a series of automated checks on the source code, ensuring that contribution meet certain quality standards.

Running these pipelines consumes significant amount of energy and adds to the climate footprint of free/libre software development.

In a quest to make Forgejo [more frugal in the use of computing](https://wimvanderbauwhede.codeberg.page/articles/frugal-computing/) in the development lifecycle, [optimizations to the CI/CD pipelines](https://codeberg.org/forgejo/forgejo/issues/5127) have been considered and a part of the work was merged.

The optimizations include [caching the playwright environment in a test image](https://codeberg.org/forgejo/forgejo/pulls/5297) and improvements to the [caching of Go dependencies](https://codeberg.org/forgejo/forgejo/pulls/5328) that improves on the caching available from the `setup-go` action that spent 10x2 minutes per job creating compressed archives. The action is [also available to other projects](https://codeberg.org/fnetX/setup-cache-go) and a dedicated contribution to the Forgejo Actions ecosystem.

## Infrastructure

On [9 September](https://codeberg.org/forgejo/discussions/issues/219) code.forgejo.org was down during 10 hours. It was overwhelmed by excessive crawling and the response time was so slow that it kept accumulating a backlog and answering every request with a timeout. On top of that it happened late at night and although it was trivially fixed by restarting Forgejo, it only happened the next morning.

This was the first significant downtime and [impacted a number of Forgejo instances](https://codeberg.org/forgejo/discussions/issues/220) that are using Forgejo Actions hosted on code.forgejo.org. A number of measures were taken to prevent that from happening:

- [Rate-limiting is imposed](https://code.forgejo.org/infrastructure/documentation/pulls/9) on the most aggressive crawlers.
- [Exclusion rules](https://code.forgejo.org/robots.txt) were defined and added to robots.txt.
- Members of the devops team are notified on their mobile when [monitoring detects a problem](https://status.forgejo.ovh/).
- An ad-hoc script was written to detect excessive timeouts during extended periods of time and automatically restart Forgejo if needed.

The script is a hack that must not stay. It proved useful a couple of times while working on strategies to reduce crawling to manageable levels. It still represents over 50% of the incoming requests but they do not impact the instance performances.

The long term solution, as code.forgejo.org audience grows, is to improve its availability. The test instances at v\*.next.forgejo.org are already [using Forgejo helm](https://code.forgejo.org/forgejo-helm/forgejo-helm/), each in [a dedicated k3s cluster](https://code.forgejo.org/infrastructure/k8s). A long lived [k8s cluster](https://code.forgejo.org/infrastructure/documentation#k8s-node) is being deployed to use the same Forgejo helm so code.forgejo.org can be migrated there. The goal is for built-in health monitoring to automatically react to an unhealthy Forgejo instance and restart it using idiomatic k8s methods instead of an ad-hoc script.

There is no urgency for the k8s cluster to replace the LXC based infrastructure. But it will take some time to improve code.forgejo.org availability in this way and the works started right away so that it has a chance to be ready before the next incident happens.

## Sustainability

A procedure for receiving payment from Codeberg on Forgejo work [was documented](https://codeberg.org/forgejo/sustainability/issues/61) and discussed. The details of the funds received and spent in 2024 were [updated](https://codeberg.org/forgejo/sustainability/pulls/60/files).

The progress of the ongoing grant [was updated](https://codeberg.org/forgejo/sustainability/src/branch/main/2022-12-01-nlnet). It was extended until the end of 2024 and got an informal agreement to increase the funding by 10K€. The legal status of the donations in Europe [was documented](https://codeberg.org/forgejo/sustainability/issues/62) with an example based on a Freelance established in Portugal, with links to do the same for other European countries.

The progress of the federation grant [was also updated](https://codeberg.org/forgejo/sustainability/src/branch/main/2022-08-01-nlnet) and a [request for payment drafted](https://codeberg.org/forgejo/sustainability/pulls/59) for 2,500€. The grant will expire 1 December 2024 and the unspent funds will be returned to NLnet where they can be used by other projects.

Questions were received from NLnet on the latest grant application and [an answer sent](https://codeberg.org/forgejo/sustainability/pulls/57) which led to [followup questions](https://codeberg.org/forgejo/sustainability/issues/63). Because of the required delay in answering those questions, the grant application was moved by a few months, to the next call.

The sustainability team elected [its first member](https://codeberg.org/forgejo/governance/src/branch/main/TEAMS.md#sustainability-team). They helped with following up with the current grants and document the progress made so far.

The relevance of creating a non profit exclusively dedicated to managing the Forgejo funds and governed by the Forgejo decision making process [was discussed](https://codeberg.org/forgejo/discussions/issues/224). A balance should be found between the burden of managing a new organization and the benefit of being more flexible than Codeberg. The current situation is problematic as a significant amount of the funds obtained in the past two years (in excess of 60,000€) will have to be returned when the grants expire by the end of 2024.

## We Forge

Forgejo is a **community of people** who contribute in an inclusive environment. We forge on an equal footing, by reporting a bug, voicing an idea in the chatroom or implementing a new feature. The following list of contributors is meant to reflect this diversity and acknowledge all contributions since the last monthly report was published. If you are missing, please [ask for an update](https://codeberg.org/forgejo/website/issues/new).

- https://codeberg.org/0ko
- https://codeberg.org/adaaa
- https://codeberg.org/aimuz
- https://codeberg.org/Alexilator
- https://codeberg.org/algernon
- https://codeberg.org/Arsen
- https://codeberg.org/avobs
- https://codeberg.org/ayakael
- https://codeberg.org/bartavi
- https://codeberg.org/benniekiss
- https://codeberg.org/bramh
- https://codeberg.org/btlogy
- https://codeberg.org/caesar
- https://codeberg.org/cbn8krgm
- https://codeberg.org/cemoktra
- https://codeberg.org/chrysn
- https://codeberg.org/Chucky2401
- https://codeberg.org/clarfonthey
- https://codeberg.org/coderofsalvation
- https://codeberg.org/CommanderRedYT
- https://codeberg.org/DamianT
- https://codeberg.org/danjones000
- https://codeberg.org/ddevault
- https://codeberg.org/delgh1
- https://codeberg.org/Dirk
- https://codeberg.org/dmowitz
- https://codeberg.org/douglasparker
- https://codeberg.org/dragon
- https://codeberg.org/earl-warren
- https://codeberg.org/el0n
- https://codeberg.org/Ember
- https://codeberg.org/Erayd
- https://codeberg.org/esainane
- https://codeberg.org/ezra
- https://codeberg.org/f403
- https://codeberg.org/floss4good
- https://codeberg.org/fnetX
- https://codeberg.org/foxy
- https://codeberg.org/fuggla
- https://codeberg.org/GamePlayer-8
- https://codeberg.org/GDWR
- https://codeberg.org/Gnu1
- https://codeberg.org/grgi
- https://codeberg.org/Gusted
- https://codeberg.org/h759bkyo4
- https://codeberg.org/IamLunchbox
- https://codeberg.org/io7m
- https://codeberg.org/JacksonBailey
- https://codeberg.org/jean-daricade
- https://codeberg.org/jerger
- https://codeberg.org/jwildeboer
- https://codeberg.org/KaKi87
- https://codeberg.org/kuolemaa
- https://codeberg.org/Kwonunn
- https://codeberg.org/lapo
- https://codeberg.org/LDericher
- https://codeberg.org/linos
- https://codeberg.org/MaddinM
- https://codeberg.org/mahlzahn
- https://codeberg.org/Mai-Lapyst
- https://codeberg.org/maltejur
- https://codeberg.org/marcellmars
- https://codeberg.org/matrss
- https://codeberg.org/mcnesium
- https://codeberg.org/mdt
- https://codeberg.org/Merith-TK
- https://codeberg.org/michael-sparrow
- https://codeberg.org/midirhee12
- https://codeberg.org/mih
- https://codeberg.org/mirkoperillo
- https://codeberg.org/mkobel
- https://codeberg.org/mlncn
- https://codeberg.org/mvdkleijn
- https://codeberg.org/n0toose
- https://codeberg.org/neonew
- https://codeberg.org/Neureka
- https://codeberg.org/nhathaway
- https://codeberg.org/nobodyinperson
- https://codeberg.org/oliverpool
- https://codeberg.org/ossie
- https://codeberg.org/paspflue
- https://codeberg.org/patdyn
- https://codeberg.org/pat-s
- https://codeberg.org/pavi
- https://codeberg.org/poVoq
- https://codeberg.org/pylixonly
- https://codeberg.org/removewingman
- https://codeberg.org/rtfb
- https://codeberg.org/rvba
- https://codeberg.org/s1m
- https://codeberg.org/sandebert
- https://codeberg.org/saurabh
- https://codeberg.org/sclu1034
- https://codeberg.org/SLASHLogin
- https://codeberg.org/s-l-s
- https://codeberg.org/SludgePhD
- https://codeberg.org/snematoda
- https://codeberg.org/solomonv
- https://codeberg.org/Squel
- https://codeberg.org/stb
- https://codeberg.org/stevenroose
- https://codeberg.org/tgy
- https://codeberg.org/thefinn93
- https://codeberg.org/thefox
- https://codeberg.org/toolforger
- https://codeberg.org/VadZ
- https://codeberg.org/viceice
- https://codeberg.org/virtulis
- https://codeberg.org/voltagex
- https://codeberg.org/wangito33
- https://codeberg.org/xenrox
- https://codeberg.org/Xinayder
- https://codeberg.org/xtex
- https://codeberg.org/yoctozepto
- https://codeberg.org/yonas
- https://translate.codeberg.org/user/aleksi
- https://translate.codeberg.org/user/ciampix
- https://translate.codeberg.org/user/emansije
- https://translate.codeberg.org/user/EssGeeEich
- https://translate.codeberg.org/user/muhaaliss
- https://translate.codeberg.org/user/Outbreak2096
- https://translate.codeberg.org/user/salif
- https://translate.codeberg.org/user/toasterbirb
- https://translate.codeberg.org/user/Zughy

A **minority of Forgejo contributors earn a living** by implementing the roadmap co-created by the Forgejo community, see [the sustainability repository](https://codeberg.org/forgejo/sustainability) for the details.
