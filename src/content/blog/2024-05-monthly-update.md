---
title: Forgejo monthly update - May 2024
publishDate: 2024-06-03
tags: ['news', 'report']
excerpt: A UI team was created by Forgejo contributors who have been at work for months on the necessary backports of bug fixes to the Forgejo v7.0 stable branch. More ambitious discussions started on the long term strategies to refactor the codebase and improve the User eXperience. Forgejo needs help to triage. If you ever create a new issue, take a moment of your time to also look at a few others and help them get to the finish line.
---

The monthly report is meant to provide a good overview of what has changed in Forgejo in the past month. If you would like to help, please get in touch in [the chatroom](https://matrix.to/#/#forgejo-chat:matrix.org) or participate in the [ongoing discussions](https://codeberg.org/forgejo/discussions).

A [UI team](https://codeberg.org/forgejo/governance/pulls/128) was created by Forgejo contributors who have been at work for months on the necessary backports of bug fixes to the Forgejo v7.0 stable branch. More [ambitious discussions](https://codeberg.org/forgejo/discussions/issues/160) started on the long term strategies to refactor the codebase and improve the User eXperience.

### Call for help

Forgejo needs help to [triage bug reports](https://codeberg.org/forgejo/discussions/issues/161) and feature requests. The growing backlog is [not a new problem](https://codeberg.org/forgejo/discussions/issues/53) and is actively worked on. If you ever create a new issue, take a moment of your time to also look at a few others and help them get to the finish line.

- For a bug fix, reading the description and trying to reproduce it manually to confirm it is still relevant will bring them closer to a resolution. Either because the developer knows it is worth their time or because it cannot be reproduced and is already fixed.

- For a feature request, read the **Needs and benefits** section and maybe ask the author to clarify. If you are convinced, add a short comment in the first person to describe how it would help you.

### UI team

The [Forgejo UI discussions](https://codeberg.org/forgejo/discussions/issues/160) led to [the creation of a UI team](https://codeberg.org/forgejo/governance/pulls/128) and four candidates ([1](https://codeberg.org/forgejo/governance/issues/131), [2](https://codeberg.org/forgejo/governance/issues/132), [3](https://codeberg.org/forgejo/governance/issues/134), [4](https://codeberg.org/forgejo/governance/issues/138)) are lined up to be the initial members to bootstrap it.

A [dedicated Matrix channel](https://matrix.to/#/#forgejo-ui:matrix.org) was created and [added to the Forgejo Matrix space](https://matrix.to/#/#forgejo:matrix.org).

In addition to discussions regarding future developments, the team took care of the [UI related features and bug fixes](https://codeberg.org/forgejo/forgejo/pulls?labels=87703), both for the development branch and for the stable branch.

## Code

### Features

Notable improvements and bug fixes:

- Groundwork for stars federated via ActivityPub ([1](https://codeberg.org/forgejo/forgejo/pulls/3494), [2](https://codeberg.org/forgejo/forgejo/pulls/3662), [3](https://codeberg.org/forgejo/forgejo/pulls/3792), [4](https://codeberg.org/forgejo/forgejo/pulls/3871))
- [Allow hiding auto generated release archives](https://codeberg.org/forgejo/forgejo/issues/3139)
- [Code Search for non-default branches and tags when repository indexer is disabled](https://codeberg.org/forgejo/forgejo/issues/3654)
- [Parse prefix from redis URI for queues](https://codeberg.org/forgejo/forgejo/issues/3836)
- [Wiki content search using git-grep](https://codeberg.org/forgejo/forgejo/pulls/3847)

[Read more](https://codeberg.org/forgejo/forgejo/pulls?labels=209916) in the pull requests.

### Improving tests

A [discussion started](https://codeberg.org/forgejo/discussions/issues/170) to improve the tests in the Forgejo codebase. Initial ideas cover the following:

- Allow integration tests outside of the tests/integration folder
- Prevent having to store binary blobs within the codebase
- Make the tests faster
- Document the testing tweaks
- Add test coverage
- Add performance testing
- Make the playwright tests easier to use

### End to end tests

The [end to end test suite](https://code.forgejo.org/forgejo/end-to-end) race conditions (in the tests of [push](https://code.forgejo.org/forgejo/end-to-end/pulls/175) and [scheduled](https://code.forgejo.org/forgejo/end-to-end/pulls/182) actions) were fixed. It still suffers from transient environmental failures (it relies or a large number of external resources), but it happens less than once a week. In some cases it can be fixed by [adding a retry](https://code.forgejo.org/forgejo/end-to-end/pulls/178).

A test for the [pull request automerge features](https://code.forgejo.org/forgejo/end-to-end/pulls/189) was added.

### Deprecating go-git

Discussions [to deprecate go-git](https://codeberg.org/forgejo/discussions/issues/164) received strong support. If Forgejo wants to support go-git, every git Feature also needs to be implemented in go-git. For example: setting git notes in the Web UI is currently not possible in go-git.

The benefits of go-git may not be worth the effort. Git is already preinstalled on many distributions. If Forgejo is installed using Docker or a package manager, Git will already be installed with it.

## Infrastructure

A new [machine](https://forgejo.org/docs/next/developer/infrastructure/#hetzner0104) was added to the Forgejo infrastructure. The capacity provisioned last year proved to be enough to sustain the increased activity since early 2024, with no slowdown or space restrictions. Even if the number of Forgejo contributors do not increase this year, testing federated features will require significantly more resources, for instance to launch a GitLab instance with ActivityPub extensions enabled.

The [forgefriends hosting request](https://codeberg.org/forgejo/discussions/issues/114) is partially complete. The https://code.forgejo.org/forgefriends/ and https://code.forgejo.org/f3/ organizations were created and allocated Forgejo Actions runners. The [F3](https://f3.forgefriends.org/) organization was migrated from the [GitLab instance](https://lab.forgefriends.org/friendlyforgeformat/). The [F3 forum](https://forum.forgefriends.org/) was migrated to [a dedicated LXC container](https://forgejo.org/docs/next/developer/infrastructure/#hetzner0104).

A semi-manual [static page hosting service](https://forgejo.org/docs/next/developer/static-pages/) dedicated to code.forgejo.org was created. It is deployed to host the [F3](https://f3.forgefriends.org) and [forgefriends](https://forgefriends.org) websites and could be used as an alternative to [Uberspace](https://forgejo.org/docs/v7.0/developer/infrastructure/#uberspace) for https://forgejo.org.

## Releases

[Forgejo v7.0.2](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/RELEASE-NOTES.md#7-0-2) and [Forgejo v7.0.3](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/RELEASE-NOTES.md#7-0-3) (fixing [vulnerabilities](https://codeberg.org/forgejo/security-announcements/issues/10)) were released.

They both rely on the [release note files](https://codeberg.org/forgejo/discussions/issues/159) that [are created at the same time as the pull requests](https://codeberg.org/forgejo/forgejo/src/commit/ade7304eea8ffdf5440adb71dfb2dcb50159379a/release-notes/8.0.0).

After a commit made its way to the v7.0 branch and was [reverted at the last minute](https://codeberg.org/forgejo/forgejo/pulls/3867) to avoid regression, it was [proposed to require testing](https://codeberg.org/forgejo/discussions/issues/168) for all non-trivial commits cherry-picked from Gitea into the stable branch.

## Dependency Management

### Tooling

The [cherry-picking tool](https://codeberg.org/forgejo/tools/src/commit/52e2ded048ecb080a92bc957743fa35086ce37e0/scripts/weekly-cherry-pick.sh) developed to keep track of commits cherry-picked from related repositories is used for:

- [the Forgejo development branch](https://codeberg.org/forgejo/forgejo/pulls/3917)
- [the Forgejo v7.0 stable branch](https://codeberg.org/forgejo/forgejo/pulls/3942)
- [the Forgejo helm](https://code.forgejo.org/forgejo-helm/forgejo-helm/pulls/506)

### OCI mirror

Forgejo maintains a set of [OCI images](https://code.forgejo.org/oci/-/packages) mirrors for the benefit of the CI, so that it is not rate limited by docker.io. They were previously manually maintained and a weekly scheduled workflow was created to [take care of it automatically](https://code.forgejo.org/forgejo/oci-mirror/src/commit/c6b1f3588f72fc9ac7a949120a084f343a716993/.forgejo/workflows/mirror.yml).

It was initially held back because of a [long standing bug](https://codeberg.org/forgejo/forgejo/issues/780) preventing the use of [docker buildx imagetools create](https://docs.docker.com/reference/cli/docker/buildx/imagetools/create/) to mirror multi-architecture OCI images. It was worked around using [skopeo](https://github.com/containers/skopeo) which provides the same feature but does not run into the Forgejo bug that would prevent it to work.

## Helm chart

The Forgejo helm chart had [one major update](https://code.forgejo.org/forgejo-helm/forgejo-helm/releases).

The Forgejo helm chart moved from [forgejo-contrib](https://codeberg.org/forgejo-contrib/forgejo-helm) to a [dedicated organization](https://code.forgejo.org/forgejo-helm).

A [Forgejo helm team](https://codeberg.org/forgejo/governance/pulls/136) was proposed, with two potential members. A [dedicated Matrix channel](https://matrix.to/#/#forgejo-helm-chart:matrix.org) was created and added to [the Forgejo Matrix space](https://matrix.to/#/#forgejo:matrix.org).

## Localization

3 new team members were onboarded: [[1]](https://codeberg.org/forgejo/governance/pulls/127), [[2]](https://codeberg.org/forgejo/governance/pulls/137). 5 batches of updates were merged containing total of 515 new translations and 1122 improvements. Traditional Chinese have seen a particularly high amount of fixes and improvements this month from a new team member. All translation changes got backported to according point releases of Forgejo v7.0, which is expected to continue receiving translation improvements.

There are still countless improvements to be made for many languages and you can help to improve the localization too. [Learn how](https://forgejo.org/docs/latest/developer/localization).

## Federation

The pull request to implement [federated repository stars](https://codeberg.org/forgejo/forgejo/pulls/1680) was split into smaller ones, five of which were merged in the development branch.

- [Federated staring of repositories](https://codeberg.org/forgejo/forgejo/pulls/1680) (in review)
- [UI to define following repos](https://codeberg.org/forgejo/forgejo/pulls/3886) (merged)
- [Finalize receive activity](https://codeberg.org/forgejo/forgejo/pulls/3871) (merged)
- [Creation of federated user](https://codeberg.org/forgejo/forgejo/pulls/3792) (merged)
- [Federation: Parse ActorId & cache FederationHost](https://codeberg.org/forgejo/forgejo/pulls/3662) (merged)
- [Validate like activities](https://codeberg.org/forgejo/forgejo/pulls/3494) (merged)

Read more in the May 2024 reports ([1](https://domaindrivenarchitecture.org/posts/2024-05-15-state-of-federation/), [2](https://domaindrivenarchitecture.org/posts/2024-05-24-state-of-federation/)).

## Governance

### Sustainability

The [discussion started](https://codeberg.org/forgejo/discussions/issues/144) on Forgejo durability in the next 10 years led to [a grant proposal](https://codeberg.org/forgejo/sustainability/pulls?state=all&labels=217359) submitted 16 May for the [Free and Open Source Software Sustainability Fund](https://www.opentech.fund/funds/free-and-open-source-software-sustainability-fund/).

A reply was expected for the NLnet grant application [submitted 1 April](https://codeberg.org/forgejo/sustainability/pulls?labels=220838) but was delayed because of the large number of applicants. The arrangements made to ensure the two grants do not overlap in time were changed. Because of this delay and on condition that an extension can be negotiated, avoidance of an overlap may no longer be necessary.

## We Forge

Forgejo is a **community of people** who contribute in an inclusive environment. We forge on an equal footing, by reporting a bug, voicing an idea in the chatroom or implementing a new feature. The following list of contributors is meant to reflect this diversity and acknowledge all contributions since the last monthly report was published. If you are missing, please [ask for an update](https://codeberg.org/forgejo/website/issues/new).

- https://codeberg.org/0ko
- https://codeberg.org/9pfs
- https://codeberg.org/aaronriedel
- https://codeberg.org/algernon
- https://codeberg.org/Andre601
- https://codeberg.org/AndrewKvalheim
- https://codeberg.org/ansemjo
- https://codeberg.org/Awlex
- https://codeberg.org/axd99
- https://codeberg.org/Beowulf
- https://codeberg.org/caesar
- https://codeberg.org/channel-42
- https://codeberg.org/clarfonthey
- https://codeberg.org/comcloudway
- https://codeberg.org/CommanderRedYT
- https://codeberg.org/crapStone
- https://codeberg.org/Crown0815
- https://codeberg.org/crystal
- https://codeberg.org/Cyborus
- https://codeberg.org/DD-P
- https://codeberg.org/deblan
- https://codeberg.org/defanor
- https://codeberg.org/Dirk
- https://codeberg.org/Drakon
- https://codeberg.org/earl-warren
- https://codeberg.org/efertone
- https://codeberg.org/el0n
- https://codeberg.org/emersion
- https://codeberg.org/f00
- https://codeberg.org/fhuberts
- https://codeberg.org/Firepup650
- https://codeberg.org/fistons
- https://codeberg.org/fnetX
- https://codeberg.org/foxy
- https://codeberg.org/Frankkkkk
- https://codeberg.org/FunctionalHacker
- https://codeberg.org/ggpsv
- https://codeberg.org/glts
- https://codeberg.org/h759bkyo4
- https://codeberg.org/hazy
- https://codeberg.org/hoppinglife
- https://codeberg.org/intelfx
- https://codeberg.org/io7m
- https://codeberg.org/jadeprime
- https://codeberg.org/JakobDev
- https://codeberg.org/james2432
- https://codeberg.org/jean-daricade
- https://codeberg.org/jerger
- https://codeberg.org/johnthomas00
- https://codeberg.org/jwildeboer
- https://codeberg.org/KaKi87
- https://codeberg.org/KalleMP
- https://codeberg.org/kdh8219
- https://codeberg.org/kenzu
- https://codeberg.org/Kladky
- https://codeberg.org/Kwonunn
- https://codeberg.org/leana8959
- https://codeberg.org/leetickett
- https://codeberg.org/Lgmrszd
- https://codeberg.org/liberodark
- https://codeberg.org/magicfelix
- https://codeberg.org/Mai-Lapyst
- https://codeberg.org/mainboarder
- https://codeberg.org/martinwguy
- https://codeberg.org/matrss
- https://codeberg.org/mguims
- https://codeberg.org/milahu
- https://codeberg.org/minion
- https://codeberg.org/mirkoperillo
- https://codeberg.org/n0toose
- https://codeberg.org/neomaitre
- https://codeberg.org/npgo22
- https://codeberg.org/NRK
- https://codeberg.org/oliverpool
- https://codeberg.org/omenos
- https://codeberg.org/openbrian
- https://codeberg.org/pensicus
- https://codeberg.org/peylight
- https://codeberg.org/PixelHamster
- https://codeberg.org/popey
- https://codeberg.org/proton-ab
- https://codeberg.org/Renich
- https://codeberg.org/roberth
- https://codeberg.org/robko23
- https://codeberg.org/sclu1034
- https://codeberg.org/scy
- https://codeberg.org/SeaswimmerTheFsh
- https://codeberg.org/sevki
- https://codeberg.org/SinTan1729
- https://codeberg.org/snematoda
- https://codeberg.org/stephanm
- https://codeberg.org/sthagen
- https://codeberg.org/sthenault
- https://codeberg.org/svoop
- https://codeberg.org/TheAwiteb
- https://codeberg.org/thefox
- https://codeberg.org/trymeout
- https://codeberg.org/tseeker
- https://codeberg.org/twenty-panda
- https://codeberg.org/ujr
- https://codeberg.org/varp0n
- https://codeberg.org/viceice
- https://codeberg.org/voltagex
- https://codeberg.org/woutput
- https://codeberg.org/xinnix
- https://codeberg.org/xunzi
- https://codeberg.org/yarikoptic
- https://codeberg.org/ZilloweZ
- https://translate.codeberg.org/user/747
- https://translate.codeberg.org/user/anonymous
- https://translate.codeberg.org/user/b1nar10
- https://translate.codeberg.org/user/Cwpute
- https://translate.codeberg.org/user/emansije
- https://translate.codeberg.org/user/enricpineda
- https://translate.codeberg.org/user/Fitik
- https://translate.codeberg.org/user/Fjuro
- https://translate.codeberg.org/user/furry
- https://translate.codeberg.org/user/hankskyjames777
- https://translate.codeberg.org/user/kita
- https://translate.codeberg.org/user/ledyba
- https://translate.codeberg.org/user/mareklach
- https://translate.codeberg.org/user/monstorix
- https://translate.codeberg.org/user/Mumulhl
- https://translate.codeberg.org/user/Mylloon
- https://translate.codeberg.org/user/NameLessGO
- https://translate.codeberg.org/user/Nifou
- https://translate.codeberg.org/user/nmmr
- https://translate.codeberg.org/user/petrcech
- https://translate.codeberg.org/user/Pi-Cla
- https://translate.codeberg.org/user/salif
- https://translate.codeberg.org/user/sunwoo1524
- https://translate.codeberg.org/user/VioletLul
- https://translate.codeberg.org/user/Werenter
- https://translate.codeberg.org/user/Wuzzy
- https://translate.codeberg.org/user/Xinayder
- https://translate.codeberg.org/user/yeziruo
- https://translate.codeberg.org/user/zyachel

A **minority of Forgejo contributors earn a living** by implementing the roadmap co-created by the Forgejo community, see [the sustainability repository](https://codeberg.org/forgejo/sustainability) for the details.
