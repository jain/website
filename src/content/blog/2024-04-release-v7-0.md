---
title: Forgejo v7.0 is available
publishDate: 2024-04-23
tags: ['releases']
release: 7.0.0
excerpt: Forgejo v7.0 is available with translations in Bulgarian, Esperanto, Filipino and Slovenian; SourceHut builds integration; support for the SHA-256 hash function in Git; source code search by default and more. It also is the first Long Term Support version and will receive updates until July 2025. The adoption of semantic versioning is the reason for the version bump from v1.21 to v7.0 and is compatible with existing tools.
---

[Forgejo v7.0](/download/) was released 23 April 2024. You will find the most interesting changes it introduces below and in a [complete list in the release notes](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/RELEASE-NOTES.md#7-0-0).

A [dedicated test instance is available](https://v7.next.forgejo.org/) to try it out. Before upgrading it is _strongly recommended_ to make a full backup as explained in the [upgrade guide](/docs/v7.0/admin/upgrade/) and carefully read _all breaking changes_ from the [release notes](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/RELEASE-NOTES.md#7-0-0). If in doubt, do not hesitate to ask for help [on the Fediverse](https://floss.social/@forgejo), or in the [chat room](https://matrix.to/#/#forgejo-chat:matrix.org).

The adoption of [Semantic Versioning 2.0.0](https://semver.org/spec/v2.0.0.html) is the reason for the version bump from `v1.21` to `v7.0` and is compatible with existing tools.

- **[Translations in four new languages](https://translate.codeberg.org/projects/forgejo/forgejo)** with a usable level of completion are available to users: Bulgarian, Esperanto, Filipino and Slovenian.

  ![Page displayed in Bulgarian](./_images/2024-04-bulgarian.png)

- **[SourceHut Builds integration](/docs/v7.0/user/webhooks/)** can be used to submit jobs to [SourceHut](https://man.sr.ht/builds.sr.ht/) on push events.

  ![SourceHut webhook](./_images/2024-04-webhook-sourcehut.png)

- **[Source code search code now available by default](/docs/v7.0/user/code-search/#basic-git-grep)** using [git grep](https://git-scm.com/docs/git-grep).

  ![Git grep search](./_images/2024-04-git-grep.png)

- **[Activity graphs](https://forgejo.org/docs/v7.0/user/repository-activity/)** show the contributors, code frequency and the recent commits in the activity tab of repositories.

  ![Contributors activity graph](./_images/2024-04-graphs.png)

- **[The wiki can be edited by any user](https://forgejo.org/docs/v7.0/user/wiki/#activation-and-permissions)** with read permissions by selecting `Allow anyone to edit the wiki` on the repository settings. The default is to only allow users with write permissions on the repository.

  ![Wiki permission settings](./_images/2024-04-real-wiki.png)

- **[Git repositories using SHA-256 are supported](https://codeberg.org/forgejo/forgejo/commit/d68a613ba8fd860863a3465b5b5945b191b87b25)**. Although Git repositories using SHA-1 are not vulnerable to [collision attacks](https://shattered.io/) since Git v2.13.0, this hash algorithm is still weak. Git [transition to a SHA-256 hash function](https://git-scm.com/docs/hash-function-transition) was decided to be trustworthy and useful in practice for at least 10 years. **As of Forgejo 7.0.2 some features are [still unreliable when using SHA-256](https://codeberg.org/forgejo/forgejo/issues/3613).**

- **[Repository badges](https://forgejo.org/docs/v7.0/user/readme-badges/)** can be used to embed information about a given repository such as the CI state, the number of issues, etc.

  ![Repository badges](./_images/2024-04-badges.png)

Read more [in the Forgejo v7.0.0 release notes](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/RELEASE-NOTES.md#7-0-0).

## 7.0 Long Term Support (LTS) and semantic versioning

The `7.0` version is the first Long Term Support (LTS) release. Critical bug and security fixes will be published in patch releases (`7.0.1`, `7.0.2`, etc.) until **July 2025**. It is also the first version to use [semantic versioning](https://semver.org/spec/v2.0.0.html). Forgejo implemented semantic versioning internally in earlier releases (for instance `v1.21` is `v6.0`) and it is now exposed publicly.

### Gitea API compatibility

Tools that are developed for the Gitea API will keep working with the new Forgejo numbering scheme. They typically make assertions on the release number to unlock new functionalities and that logic will not be impacted by a bump in the release number. The proprietary version of Gitea has a different numbering scheme (v21.X.Y, v22.X.Y) and is in a similar situation.

Read more about [Gitea compatibility in the blog post explaining the hard fork that happened in February 2024](/2024-02-forking-forward/).

### Time based release schedule

The [time based release schedule](https://forgejo.org/docs/v7.0/developer/release/#release-cycle) was established to publish a release every three months. Patch releases will be published more frequently, depending on the severity of the bug or security fixes they contain. The exact number of the release cannot be known in advance because it will be determined by the features and breaking changes it contains, as specified by the [Semantic Versioning 2.0.0](https://semver.org/spec/v2.0.0.html) specifications.

| **Date** | **Version**        | **Release date** | **End Of Life**  |
| -------- | ------------------ | ---------------- | ---------------- |
| 2024 Q1  | 7.0.0+gitea-1.22.0 | 23 April 2024    | **16 July 2025** |
| 2024 Q2  | 8.0.0+gitea-A.B.C  | 17 July 2024     | 16 October 2024  |
| 2024 Q3  | X.Y.Z+gitea-A.B.C  | 16 October 2024  | 15 January 2025  |
| 2024 Q4  | X.Y.Z+gitea-A.B.C  | 15 January 2025  | 16 April 2025    |

### 7.0-test daily releases

Releases are built daily from the latest changes found in the [v7.0/forgejo](https://codeberg.org/forgejo/forgejo/src/branch/v7.0/forgejo) development branch. They are deployed to the https://v7.next.forgejo.org instance for manual verification in case a bug fix is of particular interest ahead of the next patch release. It can also be installed locally with:

- OCI images: [root](https://codeberg.org/forgejo-experimental/-/packages/container/forgejo/7.0-test) and [rootless](https://codeberg.org/forgejo-experimental/-/packages/container/forgejo/7.0-test-rootless)
- [Binaries](https://codeberg.org/forgejo-experimental/forgejo/releases/tag/v7.0-test)

Their name stays the same but they are replaced by a new build every day.

## Webhook subsystem refactor

The webhook subsystem underwent a [substantial refactor](https://codeberg.org/forgejo/forgejo/pulls/2717) to ease the additions of new webhook types. The [SourceHut Builds](https://codeberg.org/forgejo/forgejo/issues/2714) is a driving example of how the refactored webhook architecture can be used to implement a new webhook type.

## Localization

Forgejo now got its own independent [localization foundation](https://forgejo.org/docs/v7.0/developer/localization). New [teams](https://codeberg.org/forgejo/governance/src/branch/main/TEAMS.md#localization) were formed, a Weblate [project](https://translate.codeberg.org/projects/forgejo/forgejo/) was set up. The localization no longer relies on a proprietary service.

Work on refactoring base localization to improve User eXperience and translatability is in progress. Lots of strings were updated to improve readability and be easier to understand, many got basic plural support. File and repo sizes were [made translatable](https://codeberg.org/forgejo/forgejo/pulls/2528), localization of activity heatmap [was fixed](https://codeberg.org/forgejo/forgejo/pulls/2612).

As soon as the initiative [was announced](https://forgejo.org/2024-01-monthly-update/#localization) in January, the call for participation got a great response and volunteers from around the world are working daily to improve translations. New languages were added. The ones which got active maintainers and have reached a usable level of completion [were made available](https://codeberg.org/forgejo/forgejo/pulls/2724) to the users: Bulgarian, Esperanto, Filipino, Slovenian.

Anyone is welcome to participate in improving translation [for their language](https://forgejo.org/docs/latest/developer/localization) as well as [the English base](https://forgejo.org/docs/v7.0/developer/localization-english/#contributing).

### Federation

Does `Forgejo` support federation? Not yet. Was there progress? Yes.

The monthly reports [have details](/tag/report/) on these progress.

Forges have existed for over twenty years and none of them has
achieved data portability let alone federation. Forgejo is one year
old and it will take it a some time to get there.

### Get Forgejo v7.0

See the [download page](/download/)
for instructions on how to install Forgejo, and read the
[release notes](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/RELEASE-NOTES.md#7-0-0)
for more information.

### Upgrading

Carefully read the
[breaking changes](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/RELEASE-NOTES.md#7-0-0)
section of the release notes.

The actual upgrade process is as simple as replacing the binary or container image
with the corresponding [Forgejo binary](https://codeberg.org/forgejo/forgejo/releases/tag/v7.0.0)
or [container image](https://codeberg.org/forgejo/-/packages/container/forgejo/7.0.0).
If you're using the container images, you can use the
[`7.0` tag](https://codeberg.org/forgejo/-/packages/container/forgejo/7.0)
to stay up to date with the latest `7.0.Y` patch release automatically.

Make sure to check the [Forgejo upgrade
documentation](/docs/v7.0/admin/upgrade) for
recommendations on how to properly backup your instance before the
upgrade.

### Contribute to Forgejo

If you have any feedback or suggestions for Forgejo do not hold back, it is also your project.
Open an issue in [the issue tracker](https://codeberg.org/forgejo/forgejo/issues)
for feature requests or bug reports, reach out [on the Fediverse](https://floss.social/@forgejo),
or drop into [the Matrix space](https://matrix.to/#/#forgejo:matrix.org)
([main chat room](https://matrix.to/#/#forgejo-chat:matrix.org)) and say hi!
