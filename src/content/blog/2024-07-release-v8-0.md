---
title: Forgejo v8.0 is available
publishDate: 2024-07-30
tags: ['releases']
release: 8.0.0
excerpt: Forgejo v8.0 is available with new features (support for workflow dispatch, better defaults to avoid spam on new instances etc.), a new approach to UI and UX, careful upgrade of dependencies to improve stability and security. Foundation parts for ActivityPub based federation and data portability were merged in, bringing these features closer to completion, but they're not available yet.
---

[Forgejo v8.0](/download/) was released 30 July 2024. You will find a short selection of the changes it introduces below and in a [complete list in the release notes](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/RELEASE-NOTES.md#8-0-0). It is released the [same day as Forgejo v7.0.6](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/RELEASE-NOTES.md#7-0-6) to address licensing issues impacting frontend features (APA citation format, mermaid ELK rendering and the display of contributor graphs). [Read more in the dedicated blog post](https://forgejo.org/2024-07-non-free-dependency-found/).

It comes with [a number of new features](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/RELEASE-NOTES.md#8-0-0), as usual. But the most impactful changes are of a different nature: increased stability, less random UI modifications and almost no breaking changes.

- A newly created [UI team](https://codeberg.org/forgejo/governance/src/branch/main/TEAMS.md#user-interface) engaged in a different approach and can be credited for a drastic reduction in random User Interface changes in this release.
- [A dependency dashboard](https://codeberg.org/forgejo/forgejo/issues/2779) is now used to carefully [watch over each of them](https://forgejo.org/docs/latest/developer/dependencies/), with a direct impact on stability and security.
- The breaking changes were reduced to a minimum because there is now an increased focus on backward compatibility.

If stability is more important than new features, consider using Forgejo v7.0 instead: it is a Long Term Support release that will receive bug fixes until 16 July 2025. Forgejo v8.0 will be supported until 16 October 2024, when Forgejo v9.0 is published.

A [dedicated test instance is available](https://v8.next.forgejo.org/) to try it out. Before upgrading it is _strongly recommended_ to make a full backup as explained in the [upgrade guide](/docs/v8.0/admin/upgrade/) and carefully read _all breaking changes_ from the [release notes](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/RELEASE-NOTES.md#8-0-0). If in doubt, do not hesitate to ask for help [on the Fediverse](https://floss.social/@forgejo), or in the [chat room](https://matrix.to/#/#forgejo-chat:matrix.org).

### New features

- **[Manually trigger a Forgejo Action](/docs/v8.0/user/actions/#onworkflow_dispatch)** workflow with the input provided by the user in the web interface.

  ![Page showing Forgejo Actions' workflow dispatch feature in a drop-down menu titled 'Run workflow'](./_images/2024-06-workflow-dispatch.webp)

- **[Self-registration is now disabled by default](https://codeberg.org/forgejo/forgejo/pulls/3934)** on the installation page. This is done to prevent the creation of unmaintained instances with open registration that are abused by spammers for malicious purposes.

  ![Disabled self-registration](./_images/2024-06-self-registration.webp)

- **[Generated release attachments can optionally be hidden](https://codeberg.org/forgejo/forgejo/pulls/3139)** to not be confused with the archives uploaded by the user. For instance, each Forgejo release includes [a source archive](https://codeberg.org/forgejo/forgejo/releases/download/v7.0.5/forgejo-src-7.0.5.tar.gz) which is different from the generated archive.

  ![New option](./_images/2024-06-generated-archives-1.webp)
  ![Generated archives are hidden](./_images/2024-06-generated-archives-2.webp)

Read more [in the Forgejo v8.0.0 release notes](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/RELEASE-NOTES.md#8-0-0).

### A new approach to UI and UX

The first user visible benefit of the new approach is a drastic reduction of the seemingly random User Interface changes that were frequently found in previous releases.

A gentle way of describing Forgejo User eXperience is that it is an acquired taste: it grew over the years, driven by the inspiration of the person with the keyboard in their hand. Once implemented it almost never changed. A user who started with Forgejo in 2022 can only see minor changes in 2024 and not all of them make intuitive sense. The solution to this problem is simple and was identified early on: [User Research](https://jdittrich.github.io/userNeedResearchBook/). But only in the making of Forgejo v8.0 did it get some momentum.

The time and energy of [Forgejo contributors with the skills and desire to improve the User eXperience](https://codeberg.org/forgejo/governance/src/branch/main/TEAMS.md#user-interface) went in four equally important directions:

- Conducting user testing sessions ([June 2024](https://codeberg.org/forgejo/user-research/issues/34) and [April 2024](https://codeberg.org/forgejo/user-research/src/branch/main/interviews/2024-04/template.md)) to observe how Forgejo is used. How do users work with Forgejo? What problems do they run into?
- Discussing [a new workflow for designing and implementing feature requests](https://codeberg.org/forgejo/discussions/issues/178). The focus is on the problems rather than the solution. When a user or a developer has a solution in mind, it is not uncommon that it does not solve an actual problem.
- Giving some love to the current user interface, fixing bugs and backporting them to the stable versions (e.g. [7.0.5](https://codeberg.org/forgejo/forgejo/pulls?labels=87703&milestone=6654)). Review and apply [upgrades](https://codeberg.org/forgejo/forgejo/pulls?labels=223008) of [JavaScript packages](https://codeberg.org/forgejo/forgejo/src/commit/8afdafebf9fa2cb748a13e56ff3d865675ae27b6/package.json) and [other dependencies](https://codeberg.org/forgejo/forgejo/pulls?labels=202906) used to build the UI.
- Improving the [JavaScript test coverage](https://codeberg.org/forgejo/forgejo/commits/branch/forgejo/tests/e2e) which is still currently under 10% but has seen more improvement in the past three months than in the past two years.

In a nutshell, Forgejo's goal is now to design its User eXperience based on what user needs, as demonstrated by observations, rather than what they think that they want.

### Removal of Microsoft SQL Server support

When Forgejo started almost two years ago, it focused on supporting and documenting a subset of the features present in the codebase. It made a implicit promise to its users to produce quality releases where new features could be added and bug fixed. It is a simple problem to solve as long as there are two necessary ingredients:

- Contributors with the knowledge to diagnose a problem
- Automated tests guarding the code against regressions when a new feature or a bug fix is merged in the codebase

Microsoft SQL Server never met these conditions and [discussions began early 2024](https://codeberg.org/forgejo/discussions/issues/122) to address the problem. The short version is that there is no reported use of a Forgejo instance running Microsoft SQL Server currently, and the decision was made to remove it from the codebase.

An alternative would have been to leave it, to give a chance to someone with the right skills to step up and contribute their knowledge to support Microsoft SQL Server. But that would not solve the other problem: because Microsoft SQL Server is not Free Software, it cannot be integrated in the automated tests. That would be adding a non Free Software dependency to Forgejo, which goes against its core values.

Because Forgejo is still young and none of its users rely on Microsoft SQL Server support, it can be dropped without inconveniencing anyone. As Forgejo's popularity grows, keeping the feature in the codebase would increase the probability that users rely on Microsoft SQL Server. It would then be a difficult situation for Forgejo and it is best to avoid that trap.

See also the [related section on standard formats and protocols](/2024-06-monthly-update/#standard-formats-and-protocols) in the June 2024 report.

### Less breaking changes

A special effort was made to reduce the breaking changes of this release to a minimum. For instance it would have been easier to implement [this improvement to the rootless OCI image](https://codeberg.org/forgejo/forgejo/pulls/3363) as a breaking change. But significant time was spent to figure out a way to make it backward compatible. Another example is the [new default for self-registration](https://codeberg.org/forgejo/forgejo/pulls/3934) that only applies to new installations to not require a manual intervention to change the settings.

### Improved stability

Forgejo directly depends on hundreds of software projects: OCI images are based on [Alpine Linux](https://alpinelinux.org/), markdown rendering uses [goldmark](https://github.com/yuin/goldmark), TLS certificates are obtained using [certmagic](https://github.com/caddyserver/certmagic) and the list goes on, ranging from CI tooling that only matter to Forgejo developers to user interface components such as the [editor used to write issue comments](https://microsoft.github.io/monaco-editor/).

Watching over those dependencies was an unsolved problem:

- bugs (or even security) fixes were not applied because no contributor had the time or the inclination to watch over them manually
- batch upgrades, dozens of direct dependencies at a time, occasionally happened without actually reading through the release notes of each of them, let alone evaluate the risk of regression or the new features they include

It was resolved in v8.0 with [specific tooling](https://codeberg.org/forgejo/tools/src/branch/main/scripts/wcp) and [a dependency dashboard](https://codeberg.org/forgejo/forgejo/issues/2779) updated hourly by [renovate](https://github.com/renovatebot/renovate) and watched over daily by Forgejo contributors. When a new release of a dependency is published, it is immediately proposed for review and the decision to upgrade is made in accordance to the new [dependency management process](/docs/v8.0/developer/dependencies/). The tooling made it possible and sustainable to observe the evolution of hundred of dependencies.

The immediate benefits are:

- more stability as batch upgrades no longer happen with the associated risk of regression
- bugs and security fixes found in dependencies are applied without undue delay, and backported to stable releases

### Gitea compatibility

Forgejo v8.0 was manually tested to be compatible with Gitea v1.22. Users reported successful upgrades of an instance of Gitea v1.22 to the development version of Forgejo v8.0. In addition, [automated upgrade tests from Gitea v1.22 to Forgejo v8.0](https://code.forgejo.org/forgejo/end-to-end/pulls/205) were implemented and are run prior to each Forgejo release.

- An instance running Gitea versions up to v1.21 can be upgraded to Forgejo v7.0 or v8.0
- An instance running Gitea v1.22 can be upgraded to Forgejo v8.0

Read more about [Gitea compatibility in the blog post explaining the hard fork that happened in February 2024](/2024-02-forking-forward/).

### Release schedule and Long Term Support

The [time based release schedule](/docs/v8.0/developer/release/#release-cycle) was established to publish a release every three months. Patch releases will be published more frequently, depending on the severity of the bug or security fixes they contain.

| **Date** | **Version** | **Release date** | **End Of Life**  |
| -------- | ----------- | ---------------- | ---------------- |
| 2023 Q4  | 1.21.1-0    | 26 November 2023 | 17 July 2024     |
| 2024 Q1  | 7.0.0       | 23 April 2024    | **16 July 2025** |
| 2024 Q2  | **8.0.0**   | 30 July 2024     | 16 October 2024  |
| 2024 Q3  | 9.0.0       | 16 October 2024  | 15 January 2025  |

### 8.0-test daily releases

Releases are built daily from the latest changes found in the [v8.0/forgejo](https://codeberg.org/forgejo/forgejo/src/branch/v8.0/forgejo) development branch. They are deployed to the https://v8.next.forgejo.org instance for manual verification in case a bug fix is of particular interest ahead of the next patch release. It can also be installed locally with:

- OCI images: [root](https://codeberg.org/forgejo-experimental/-/packages/container/forgejo/8.0-test) and [rootless](https://codeberg.org/forgejo-experimental/-/packages/container/forgejo/8.0-test-rootless)
- [Binaries](https://codeberg.org/forgejo-experimental/forgejo/releases/tag/v8.0-test)

Their name stays the same but they are replaced by a new build every day.

## Localization

The [localization team](https://codeberg.org/forgejo/governance/src/branch/main/TEAMS.md#localization) brought a batch of translations weekly, from the Weblate [project](https://translate.codeberg.org/projects/forgejo/forgejo/). A particular effort was made to backport all of them to Forgejo v7.0.

The work on refactoring base localization to improve User eXperience and translatability was also ported to v7.0, when there was no risk of regressions.

Anyone is welcome to participate in improving translation [for their language](/docs/v8.0/developer/localization) as well as [the English base](/docs/v8.0/developer/localization-english/#contributing).

### Federation

Does `Forgejo` support federation? Not yet. Was there progress? Yes.

Building blocks for both [ActivityPub federation](https://codeberg.org/forgejo/forgejo/pulls/1680) and [data portability improvements](https://codeberg.org/forgejo/forgejo/pulls/3590) were merged into the codebase. They are not yet used for any user visible feature but they are a stepping stone. Their implementation was made significantly easier by the hard fork because they can rely on a codebase that is better tested.

The ActivityPub based communication between two Forgejo instances is used by a new internal test scenario where adding a star to a repository on one instance also adds a star on a federated repository on the other instance.

Read more about [federation](/2024-06-monthly-update/#federation) and [data portability](/2024-06-monthly-update/#data-portability) in the June 2024 [Forgejo monthly update](https://forgejo.org/2024-06-monthly-update/).

### Get Forgejo v8.0

See the [download page](/download/)
for instructions on how to install Forgejo, and read the
[release notes](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/RELEASE-NOTES.md#8-0-0)
for more information.

### Upgrading

Carefully read the
[breaking changes](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/RELEASE-NOTES.md#8-0-0)
section of the release notes.

The actual upgrade process is as simple as replacing the binary or container image
with the corresponding [Forgejo binary](https://codeberg.org/forgejo/forgejo/releases/tag/v8.0.0)
or [container image](https://codeberg.org/forgejo/-/packages/container/forgejo/8.0.0).
If you're using the container images, you can use the
[`8.0` tag](https://codeberg.org/forgejo/-/packages/container/forgejo/8.0)
to stay up to date with the latest `8.0.Y` patch release automatically.

Make sure to check the [Forgejo upgrade
documentation](/docs/v8.0/admin/upgrade) for
recommendations on how to properly backup your instance before the
upgrade.

### Contribute to Forgejo

If you have any feedback or suggestions for Forgejo do not hold back, it is also your project.
Open an issue in [the issue tracker](https://codeberg.org/forgejo/forgejo/issues)
for feature requests or bug reports, reach out [on the Fediverse](https://floss.social/@forgejo),
or drop into [the Matrix space](https://matrix.to/#/#forgejo:matrix.org)
([main chat room](https://matrix.to/#/#forgejo-chat:matrix.org)) and say hi!

### Donate

Forgejo is proud to be [funded transparently](https://codeberg.org/forgejo/sustainability/). Since a few days, we additionally accept donations from our users through [our Forgejo Liberapay team](https://liberapay.com/forgejo). If you appreciate Forgejo, consider setting up a donation to help going forward.
Liberapay is a French non-profit dedicated to crowdfunding with predictable income, allowing transfers via multiple payment options and providers with a comparably low fee. It was already possible to [donate to Codeberg e.V.](https://docs.codeberg.org/improving-codeberg/donate/) (you can still do in case the Liberapay option does not work out for you), and part of the funding was used to [compensate for work on Forgejo](https://codeberg.org/forgejo/sustainability/#forgejo-resources-per-year). However, the Liberapay team now allows for money to go directly to developers without a round-trip to Codeberg.

Additionally, Liberapay allows for a steady and reliable funding stream next to other options, a crucial aspect for our project. The distribution of funds through Liberapay is [transparently controlled using our decision-making process](https://codeberg.org/forgejo/governance/src/branch/main/TEAMS.md#liberapay-team-members), and Forgejo contributors are encouraged to consider applying to benefit from this funding opportunity.
Thank you for using Forgejo and considering a donation, in case your financial situation allows you to.

### Forgejo Upgrade Party and Get-Together

Let's upgrade together!

The Forgejo v8 release is available. We are looking forward to meeting with you via OpenTalk, providing a space for admins and developers to assist each other. We are excited to learn from your setups and the challenges you encounter to further improve Forgejo in the future.

[Read the full invite on the Codeberg Event Calendar!](https://codeberg.codeberg.page/Events/events/2024/08-02-forgejo-upgrade-party/)
