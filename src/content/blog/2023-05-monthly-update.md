---
title: Forgejo monthly update - May 2023
publishDate: 2023-06-07
tags: ['news', 'report']
excerpt: NLnet awarded a grant to Forgejo to develop its release process and additional features, starting June 2023. User blocking was implemented to protect repositories you own from unwanted interactions, an essential self-service moderation tool. An extended installation section was added to the documentation as well as a user and admin documentation for Forgejo Actions.
---

Forgejo was [awarded grant](https://nlnet.nl/project/Forgejo/) from https://nlnet.nl/ which will allow Codeberg e.V. and community members to get funding for improving the Forgejo release process and develop additional features, starting June 2023. It complements the on-going grant dedicated to federation: the features it will provide can only be deployed if releases are published and tested to work in a secure environment.

The Codeberg moderation team is facing daily challenges that are a heavy burden. A new feature was implemented to allow [users to block someone](/docs/v1.20/user/blocking-user) in the repositories they own, allowing them to mitigate some of the most common problems. This not only helps relieve the pressure on the moderation team of any public Forgejo instance, it also is an essential building block for federation because it creates more opportunities for unwanted attention.

## Development

Forgejo depends on Gitea and over a hundred other Go packages. But Gitea is not provided or designed as a package and that creates unique challenges when upgrading, which is done on a weekly basis. Forgejo developed [workflows](/docs/v1.20/developer/WORKFLOW) since its inception to reduce the workload.

Every automated test reduces the scrutiny required from Forgejo contributors when upgrading Gitea. This is specially challenging when dealing with the authentication system because it has almost no test coverage. A mechanism to allow for [dependency injection](https://codeberg.org/forgejo/forgejo/pulls/8030) in integration tests was added as well as a simple OAuth2 test to demonstrate how to use it.

When upgrading Forgejo, database migrations are applied using a set of files and a sequential numbering to ensure their consistency. Forgejo specific database modifications [cannot conveniently be inserted in that sequence](https://codeberg.org/forgejo/discussions/issues/32). Instead, [a separate migration directory](https://codeberg.org/forgejo/forgejo/pulls/795) was created and manages a set of tables prefixed with `forgejo_`. It was used in the implementation of the user blocking feature.

### User moderation feature

Moderation features now [have their dedicated feature branch](/docs/v1.20/developer/WORKFLOW#moderation). It will contain tools that fall in two categories: self-service to help users in the simpler cases and helpers for a dedicated moderation team with admin privileges who take care of the entire Forgejo instance.

The first to [land](https://codeberg.org/forgejo/forgejo/pulls/540) was [user blocking](/docs/v1.20/user/blocking-user) and [organization level blocking](https://codeberg.org/forgejo/forgejo/pulls/802) is in progress. Blocking another user is desirable if they are acting maliciously or are spamming your repository.

### Documentation

A new section was added to the documentation with a very detailed explanation of how to [install and setup Forgejo using a binary](/docs/v1.20/admin/installation#installation-from-binary). And also minimal install instructions for [container images](/docs/v1.20/admin/installation#installation-with-docker).

Two other sections were also added for the [admin](/docs/v1.20/admin/actions) and [user](/docs/v1.20/user/actions) documentation of Forgejo Actions. It is still work in progress but provides the basics to [get started](/docs/v1.20/user/actions#quick-start) and replaces the [older blog post](/2023-02-27-forgejo-actions).

### Forgejo Actions

A CI configuration for Forgejo was [created based on Forgejo Actions](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/.forgejo/workflows). Although it has been tested to work, the [Woodpecker CI](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/.woodpecker) is still used to validate pull requests. It will be run in parallel as soon as [Codeberg enables Forgejo Actions](https://codeberg.org/forgejo/discussions/issues/36).

Forgejo extensively uses [LXC](https://linuxcontainers.org/lxc/) in the Forgejo runner. A set of shell scripts [named lxc-helpers](https://code.forgejo.org/forgejo/lxc-helpers/) were developed, [tested](https://code.forgejo.org/forgejo/lxc-helpers/actions?workflow=&state=closed) (with Forgejo Actions) and documented in a separate repository. They are now used in various other contexts (the [setup-forgejo](https://code.forgejo.org/actions/setup-forgejo) action or the [Ansible playbooks](https://lab.enough.community/main/infrastructure/-/tree/master/playbooks/forgejo) used to deploy Forgejo hardware).

The [Forgejo runner](https://code.forgejo.org/forgejo/runner) saw [more releases](https://code.forgejo.org/forgejo/runner/releases) and is turning more secure (releases are now GPG signed), stable and configurable.

### Simpler and more secure release process

The release process was [revisited](https://codeberg.org/forgejo/website/pulls/230), taking the opportunity of the [brand new Forgejo runner](https://code.forgejo.org/forgejo/runner) to do so. In a nutshell it is a set of [Forgejo Actions](/docs/v1.20/user/actions) workflows that behave differently depending on the variables/secrets that are available. The repository is automatically mirrored in different organizations (experimental, integration, release) that have different variables/secrets to perform their expected role.

Here is how it goes for the Forgejo runner:

- when in the `forgejo-integration` organization, [build the release](https://code.forgejo.org/forgejo/runner/src/branch/main/.forgejo/workflows/build-release.yml)
- when in the `forgejo-release` organization, safely hosted behind a VPN, use the GPG release key & secrets to sign and copy the release that was created in the `forgejo-integration` organization to [publish the binary release](https://code.forgejo.org/forgejo/runner/src/branch/main/.forgejo/workflows/publish-binary.yml) and [the container image](https://code.forgejo.org/forgejo/runner/src/branch/main/.forgejo/workflows/publish-container-image.yml)

## Hardware

The [devops team](https://codeberg.org/forgejo/governance/src/branch/main/TEAMS.md#devops) completed the deployment of the new hardware at `octopuce.forgejo.org`. It is now in production and part of the [new release process](https://codeberg.org/forgejo/website/pulls/230) that was used to produce the latest releases of the [Forgejo runner](https://code.forgejo.org/forgejo/runner/releases). It provides a VPN for the release team to secure the last step which involves a workflow to sign the binaries.

## Funding

The NLnet grant application focused on producing quality Forgejo releases that are essential for federation to happen was [granted](https://codeberg.org/forgejo/sustainability/issues/1#issuecomment-921831). It will provide funding to Codeberg e.V., https://codeberg.org/oliverpool, https://codeberg.org/crystal and https://codeberg.org/earl-warren for their work starting in June 2023. It complements the on-going grant that provides funding for developing moderation features.

The general assembly of Codeberg e.V. renewed its support to Forgejo and decided to allocate funding in 2023 to further Forgejo.

## Reconciliation and Moderation

The `archive@matrix.org` bot entered the Forgejo chatrooms and started operating. Some Forgejo community members did not approve and asked the [moderation team](https://codeberg.org/forgejo/governance/src/branch/main/TEAMS.md#moderation) to ban it. A [discussion began](https://codeberg.org/forgejo/discussions/issues/37) and will be concluded by a decision to accept or reject this bot.

A few months ago a chatroom was created and is dedicated to the reconciliation of Forgejo community members. Making peace after a dispute is not easy but it happened and sets an example that will hopefully inspire more reconciliations. Until that happens the moderation team offers to act as a buffer between community members so they can contribute to Forgejo, working side by side without being forced into stressful interactions.

## Licensing

Following the decision that [Forgejo will accept copylefted contributions](https://codeberg.org/forgejo/governance/pulls/20), a blog post was [published to explain](/2023-06-copyleft) the reasoning. The next step is an agreement to [accept contributions compatible with GPLv3-or-later](https://codeberg.org/forgejo/governance/pulls/24).

## We Forge

Forgejo is a **community of people** who contribute in an inclusive environment. We forge on an equal footing, by reporting a bug, voicing an idea in the chatroom or implementing a new feature. The following list of contributors is meant to reflect this diversity and acknowledge all contributions since the last monthly report was published. If you are missing, please [ask for an update](https://codeberg.org/forgejo/website/issues/new).

- https://codeberg.org/buhtz
- https://codeberg.org/caesar
- https://codeberg.org/circlebuilder
- https://codeberg.org/CleoMenezesJr
- https://codeberg.org/crystal
- https://codeberg.org/dachary
- https://codeberg.org/DanielGibson
- https://codeberg.org/eagle_idea
- https://codeberg.org/earl-warren
- https://codeberg.org/editfund-founder
- https://codeberg.org/ekaitz-zarraga
- https://codeberg.org/fluzz
- https://codeberg.org/fnetX
- https://codeberg.org/fr33domlover
- https://codeberg.org/GamePlayer-8
- https://codeberg.org/greenpete
- https://codeberg.org/Gusted
- https://codeberg.org/gwymor
- https://codeberg.org/JakobDev
- https://codeberg.org/KaKi87
- https://codeberg.org/kryptonian
- https://codeberg.org/macfanpl
- https://codeberg.org/matiaslavik
- https://codeberg.org/Mikaela
- https://codeberg.org/n0toose
- https://codeberg.org/oliverpool
- https://codeberg.org/paintgoblin
- https://codeberg.org/prcek
- https://codeberg.org/Sevichecc
- https://codeberg.org/testserver22
- https://codeberg.org/tgy
- https://codeberg.org/thatonecalculator
- https://codeberg.org/timmwille
- https://codeberg.org/viceice
- https://codeberg.org/wxiaoguang
- https://codeberg.org/xy
- https://codeberg.org/zander

A **minority of Forgejo contributors earn a living** by implementing the roadmap co-created by the Forgejo community, see [the sustainability repository](https://codeberg.org/forgejo/sustainability) for the details.
