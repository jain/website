---
title: Forgejo welcomes copyleft contributions
publishDate: 2023-06-06
tags: ['news']
excerpt: The world of Free Software includes copylefted software and Git is one of the most widely known example. Distributing Forgejo under a permissive license means that all copyleft authors and software are excluded from participating in its development. The community made a decision to change that status quo and welcome copyleft contributions, just like Git.
---

Developers who choose to publish their work under a copyleft license are excluded from participating in software that is published under a permissive license. That is at the opposite of the [core values](https://codeberg.org/forgejo/governance/src/branch/main/MISSION.md#values) of the Forgejo project and [it was decided to also accept copylefted](https://codeberg.org/forgejo/governance/pulls/20) contributions. Will this change anything for Forgejo users? Not a thing since they already use and install one of the most successful copyleft software alongside Forgejo: Git. Will this change anything for Forgejo developers? Not a thing since they can keep contributing under the license of their choice. **Just like Git, Forgejo will be copyleft.**

## Forgejo users already welcome copyleft

Forgejo is a [software forge](<https://en.wikipedia.org/wiki/Forge_(software)>), an online development environment, based on [Git](https://en.wikipedia.org/wiki/Git). **Both Forgejo and Git must be installed together**, either as individual binaries or bundled into the [official container images](https://codeberg.org/forgejo/-/packages/container/forgejo/1.19-rootless). The license of Git is [GNU GPLv2](https://git-scm.com/about/free-and-open-source) which is a [copyleft license](https://www.gnu.org/licenses/copyleft.html). Although the Forgejo codebase includes basic support for [go-git](https://github.com/go-git/go-git), a Go package distributed under a permissive license that can be used in place of Git, it is not supported or packaged because it is [not fully compatible](https://github.com/go-git/go-git/blob/master/COMPATIBILITY.md) and could corrupt Git repositories.

There are legal obligations attached to copyleft software which are, in a nutshell, to offer the source code with the binary under the same license. For instance, if an employee was to distribute a Git binary to someone else, the organization they work for can be required to also provide the [complete and corresponding source](https://sfconservancy.org/copyleft-compliance/glossary.html). And if they fail to do so, they may loose all their rights under the license, at least until the copyright holders agree to grant them back. For this reason some organizations exclude copyelfted software entirely. However, **when they use Git, it means the legal counsel has no objection to copylefted software.**

## What will change in Forgejo?

The license of Forgejo will change to be copyleft when a copylefted work is merged. The most likely candidate is, for instance, when a Go package is discovered that provides an interesting functionality.

Before it happens a decision will be made by the community to choose a copyleft license, in accordance to the [Forgejo decision making process](https://codeberg.org/forgejo/governance/src/branch/main/DECISION-MAKING.md). The chosen license will apply to Forgejo as a whole (binary and sources) but each file within the codebase will retain its own license. This is nothing new as Forgejo [already includes various licenses](https://code.forgejo.org/assets/js/licenses.txt) and contributors are expected to agree to the [Developer Certificate of Origin](/docs/v1.20/developer/DCO).

Developers who feel strongly about exclusively publishing their work under a permissive license can keep doing so when working on Forgejo. By the terms of this permissive license, they accept that their work can be sublicensed and redistributed under a proprietary license. And they also accept that it can be sublicensed and redistributed as part of Forgejo, under a copyleft license.
