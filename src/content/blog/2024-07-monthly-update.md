---
title: Forgejo monthly update - July 2024
publishDate: 2024-07-31
tags: ['news', 'report']
excerpt: A non-free JavaScript library was found in the project's dependency structure, and the entire component that relied on it was re-implemented. Forgejo v8.0 is available with new features, a new approach to UI and UX, careful upgrade of dependencies to improve stability and security. Foundation parts for ActivityPub based federation and data portability were merged in.
---

The monthly report is meant to provide a good overview of what has changed in Forgejo in the past month. If you would like to help, please get in touch in [the chatroom](https://matrix.to/#/#forgejo-chat:matrix.org) or participate in the [ongoing discussions](https://codeberg.org/forgejo/discussions).

[A non-free JavaScript library was found](https://forgejo.org/2024-07-non-free-dependency-found/) in the project's dependency structure, and the entire component that relied on it was re-implemented. The new versions 8.0.0 and 7.0.6 were released without this library. This step is important to meet the core values of Forgejo.

[Forgejo v8.0 is available](https://forgejo.org/2024-07-release-v8-0/) with new features (support for workflow dispatch, better defaults to avoid spam on new instances etc.), a new approach to UI and UX, careful upgrade of dependencies to improve stability and security. Foundation parts for ActivityPub based federation and data portability were merged in.

## Forgejo v8.0 release

A gentle way of describing Forgejo User eXperience is that it is an acquired taste: it grew over the years, driven by the inspiration of the person with the keyboard in their hand. Once implemented it almost never changed. A user who started with Forgejo in 2022 can only see minor changes in 2024 and not all of them make intuitive sense. The solution to this problem is simple and was identified early on: [User Research](https://jdittrich.github.io/userNeedResearchBook/). But only in the making of Forgejo v8.0 did it get some momentum.

A special effort was also made to reduce the breaking changes of this release to a minimum. For instance it would have been easier to implement [this improvement to the rootless OCI image](https://codeberg.org/forgejo/forgejo/pulls/3363) as a breaking change. But significant time was spent to figure out a way to make it backward compatible. Another example is the [new default for self-registration](https://codeberg.org/forgejo/forgejo/pulls/3934) that only applies to new installations to not require a manual intervention to change the settings.

## Licensing

The release of Forgejo v8.0 was delayed because it was discovered that a non-free dependency of a dependency initially created for Gitea was loaded into the project.

The [dependency already existed](https://github.com/go-gitea/gitea/commit/81cfe243f9cb90b0a75de7a03bb2d264c97f0036#diff-7ae45ad102eab3b6d7e7896acd08c427a9b25b346470d7bc6507b6481575d519R9) at the time of the fork from Gitea and was therefore included in Forgejo from the beginning. The commitment of Forgejo is to always be [free as in freedom, open source and a community-first product](https://codeberg.org/forgejo/governance/src/branch/main/MISSION.md#values). Non-free dependencies and distribution licenses are incompatible with the values of Forgejo. Therefore, it was of high importance to remove the problematic dependency

Read more in [the "Non-free dependency discovered in Forgejo and removed"](/2024-07-non-free-dependency-found/) blog post.

In [June last year](https://codeberg.org/forgejo/governance/pulls/24) an agreement [was reached](https://codeberg.org/forgejo/governance/src/branch/main/AGREEMENTS.md#licensing) by which:

> Forgejo accepts contributions compatible with the GPLv3-or-later license. The license under which Forgejo is distributed will be changed upon the acceptance of such contributions. See the LICENSE file for the current license.

This has not happened yet but there now are pull requests in flight that have copylefted code in them. [Discussions started](https://codeberg.org/forgejo/discussions/issues/192) to find the best way to move forward.

## Design

The [forgejo/design](https://codeberg.org/forgejo/design) repository was created. Design being wider than the visual appearance, it's about User eXperience, workflows, efficiency, and even technical aspects. It is the interface between the UI and User Research Teams, they iterate on features, gather data, exchange ideas.

The envisioned workflow is:

- Feature requests are filed to Forgejo.
- When some of them sound interesting or related, a new issue in forgejo/design is created to coordinate the work on a feature.
- New insights (e.g. from user research, but also from duplicate or related issues) can be quickly referenced in the related issue.
- The related Git repository is used for developing the actual state of the feature. People can propose changes which are documented in markdown files.
- Whenever a design is ready, it can be referenced again in the Forgejo repository and used for implementation.
- When a developer wants to start working on something and would like to get design feedback, they also open an issue in the design repo to gain input UI wise and from user research.
- Finally, the design repo should contain guidelines for consistency as a developer resource.

## Helm chart

It is used to deploy the test instances v8.next.forgejo.org and v9.next.forgejo.org using a [dedicated repository](https://code.forgejo.org/infrastructure/k8s) in the newly created [infrastructure organization](https://code.forgejo.org/infrastructure) that contains the code automating the updates to the Forgejo infrastructure.

The Forgejo helm chart had [four patch updates](https://code.forgejo.org/forgejo-helm/forgejo-helm/releases), one for [7.0.2](https://code.forgejo.org/forgejo-helm/forgejo-helm/releases/tag/v7.0.2) which depends on Forgejo v7.0.5, one for [7.0.3](https://code.forgejo.org/forgejo-helm/forgejo-helm/releases/tag/v7.0.3) and another for [5.1.3](https://code.forgejo.org/forgejo-helm/forgejo-helm/releases/tag/v5.1.3).

A new major version, [8.1.0](https://code.forgejo.org/forgejo-helm/forgejo-helm/releases/tag/v8.1.0) was published and includes [Forgejo v8.0.0](https://forgejo.org/2024-07-release-v8-0/) as well as changes from the Gitea chart.

## Localization

Malayalam and Serbian translations, although incomplete, existed in the past but were deleted from the repository over a year ago, before the Forgejo localization team was established. They were stored on a proprietary translation platform, waiting to reach a percentage of completion before being added back. But Forgejo works differently: all translations are present in the repository and only a selected subset is displayed to the user, when they reach an acceptable completion percentage. The once deleted files for Malayalam and Serbian were restored in the repository to help resume the translation effort.

## Release notes assistant

Forgejo release notes do not fit the popular trend that consists on enforcing [conventional commits](https://www.conventionalcommits.org/) so that [tools](https://github.com/orhun/git-cliff) can collect them because:

- **The unit of change in Forgejo is the pull request, not the commit.** It is not uncommon for a pull request to contain multiple commits that are related but independent from each other. Squashing them into a single commit is not an option. There are diverging opinions on the matter and enforcing a merge policy where pull requests are always squashed into a single commit leads to a nicely flat commit history. But Forgejo made a different choice.
- **A single commit or pull request may require multiple release note lines.** When upgrading a dependency by modifying one line in the `go.mod` file leads to user facing changes, each of them needs to be described in the release notes. Forgejo relies on hundreds of other Free Software projects and although most of them are plumbing that the Forgejo user do not need to be aware of, others are very visible such as the web editor.
- **Release notes need manual editing.** No matter how good and disciplined the author of a pull request is, there will be typos and unification problems that are best addressed when preparing the release, at the very last stage.

Discussions started early 2024 ([here](https://codeberg.org/forgejo/discussions/issues/159) and [there](https://codeberg.org/forgejo/discussions/issues/155)) to distribute the release notes workload to the author of each pull request rather than relying on a single person manually doing the same a few days before the release. As the number of contributors to Forgejo increased significantly early 2024, the quality of the [v7.0.0 release notes](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/RELEASE-NOTES.md#7-0-0) was sub-standard and a solution had to be found.

Read more in the [Scaling out Forgejo release notes](https://codeberg.org/forgejo/discussions/issues/197) postmortem.

## Sustainability

Donations are now accepted for the [Forgejo Liberapay team](https://liberapay.com/forgejo). Liberapay is a French non-profit dedicated to crowdfunding with predictable income, allowing transfers via multiple payment options and providers with a comparably low fee. It was already possible to [donate to Codeberg e.V.](https://docs.codeberg.org/improving-codeberg/donate/), and part of the funding was used to [compensate for work on Forgejo](https://codeberg.org/forgejo/sustainability/#forgejo-resources-per-year). However, the Liberapay team now allows for money to go directly to developers without a roundtrip to Codeberg. Additionally, Liberapay allows for a steady and reliable funding stream next to other options, a crucial aspect for our project. The distribution of funds through Liberapay is [transparently controlled using our decision-making process](https://codeberg.org/forgejo/governance/src/branch/main/TEAMS.md#liberapay-team-members), and Forgejo contributors are encouraged to consider applying to benefit from this funding opportunity.

The NLnet grant application [submitted 1 April](https://codeberg.org/forgejo/sustainability/pulls?labels=220838) passed the first round of review and will have to answer questions to qualify for the second round.

Drawing upon [previous sustainability discussions](https://codeberg.org/forgejo/discussions/issues/144), a [grant application was drafted](https://codeberg.org/avobs/sustainability/src/branch/main/2024-07-22%20STF/application_text.md) for the [Sovereign Tech Fund](https://www.sovereigntechfund.de/). It seems to be a good fit for Forgejo, and in particular for the a similar project outline already proposed to OTF. Unlike the OTF call, STF does not have deadlines for general funding requests. It could be submitted before mid-August.

Discussions started to etablish a sustainability team, tasked to map out and implement a strategy on how to make Forgejo a durable endeavour over the next years. It seems that the project is at a point where it could use a concerted effort in this direction, even if, or maybe because, it isn't a very popular matter.

## We Forge

Forgejo is a **community of people** who contribute in an inclusive environment. We forge on an equal footing, by reporting a bug, voicing an idea in the chatroom or implementing a new feature. The following list of contributors is meant to reflect this diversity and acknowledge all contributions since the last monthly report was published. If you are missing, please [ask for an update](https://codeberg.org/forgejo/website/issues/new).

- https://codeberg.org/0ko
- https://codeberg.org/ajtatum
- https://codeberg.org/alexandria
- https://codeberg.org/algernon
- https://codeberg.org/anbraten
- https://codeberg.org/arija
- https://codeberg.org/avobs
- https://codeberg.org/awiteb
- https://codeberg.org/banaanihillo
- https://codeberg.org/behm
- https://codeberg.org/bencurio
- https://codeberg.org/benedictjohannes
- https://codeberg.org/benniekiss
- https://codeberg.org/Beowulf
- https://codeberg.org/bramh
- https://codeberg.org/caesar
- https://codeberg.org/chizutan5
- https://codeberg.org/clarfonthey
- https://codeberg.org/clemensgeibel
- https://codeberg.org/codebert
- https://codeberg.org/Crown0815
- https://codeberg.org/Cyborus
- https://codeberg.org/Darthagnon
- https://codeberg.org/dcz
- https://codeberg.org/depeo
- https://codeberg.org/Dirk
- https://codeberg.org/dleberre
- https://codeberg.org/dstensnes
- https://codeberg.org/earl-warren
- https://codeberg.org/efertone
- https://codeberg.org/el0n
- https://codeberg.org/Ember
- https://codeberg.org/ethanaobrien
- https://codeberg.org/evrial
- https://codeberg.org/floss4good
- https://codeberg.org/fnetX
- https://codeberg.org/GDWR
- https://codeberg.org/grosmanal
- https://codeberg.org/Gusted
- https://codeberg.org/hazy
- https://codeberg.org/hugorodrigues
- https://codeberg.org/iaxat
- https://codeberg.org/ikuyo
- https://codeberg.org/Ilyas0Iks
- https://codeberg.org/JakobDev
- https://codeberg.org/jalil
- https://codeberg.org/jdittrich
- https://codeberg.org/jean-daricade
- https://codeberg.org/jerger
- https://codeberg.org/jthvai
- https://codeberg.org/jwildeboer
- https://codeberg.org/KaKi87
- https://codeberg.org/kita
- https://codeberg.org/kpfleming
- https://codeberg.org/Kwonunn
- https://codeberg.org/l_austenfeld
- https://codeberg.org/leana8959
- https://codeberg.org/liberodark
- https://codeberg.org/LunarLambda
- https://codeberg.org/mahlzahn
- https://codeberg.org/Mai-Lapyst
- https://codeberg.org/marcellmars
- https://codeberg.org/martinwguy
- https://codeberg.org/meaz
- https://codeberg.org/mrwsl
- https://codeberg.org/mysticmode
- https://codeberg.org/n0toose
- https://codeberg.org/nhathaway
- https://codeberg.org/oelmekki
- https://codeberg.org/patdyn
- https://codeberg.org/pointlessone
- https://codeberg.org/poVoq
- https://codeberg.org/rdvn
- https://codeberg.org/rohitsharma09
- https://codeberg.org/sbatial
- https://codeberg.org/shisui
- https://codeberg.org/Skivling
- https://codeberg.org/slingamn
- https://codeberg.org/snematoda
- https://codeberg.org/solomonv
- https://codeberg.org/strk
- https://codeberg.org/tepozoa
- https://codeberg.org/thefox
- https://codeberg.org/thepaperpilot
- https://codeberg.org/ThetaDev
- https://codeberg.org/thetredev
- https://codeberg.org/toolforger
- https://codeberg.org/tseeker
- https://codeberg.org/twenty-panda
- https://codeberg.org/uda
- https://codeberg.org/viceice
- https://codeberg.org/wetneb
- https://codeberg.org/Xinayder
- https://codeberg.org/xvello
- https://codeberg.org/zotan
- https://translate.codeberg.org/user/anonymous
- https://translate.codeberg.org/user/b1nar10
- https://translate.codeberg.org/user/balinteus
- https://translate.codeberg.org/user/ch0ccyra1n
- https://translate.codeberg.org/user/ciampix
- https://translate.codeberg.org/user/emansije
- https://translate.codeberg.org/user/EssGeeEich
- https://translate.codeberg.org/user/ewm
- https://translate.codeberg.org/user/Fjuro
- https://translate.codeberg.org/user/hankskyjames777
- https://translate.codeberg.org/user/kdh8219
- https://translate.codeberg.org/user/manolosd
- https://translate.codeberg.org/user/meskobalazs
- https://translate.codeberg.org/user/Nifou
- https://translate.codeberg.org/user/revi
- https://translate.codeberg.org/user/salif
- https://translate.codeberg.org/user/Wuzzy
- https://translate.codeberg.org/user/ZDev
- https://translate.codeberg.org/user/Zughy

A **minority of Forgejo contributors earn a living** by implementing the roadmap co-created by the Forgejo community, see [the sustainability repository](https://codeberg.org/forgejo/sustainability) for the details.
