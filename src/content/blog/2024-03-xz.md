---
title: Impact of CVE-2024-3094 on Forgejo
publishDate: 2024-03-31
tags: ['news', 'security']
excerpt: No direct impact of the xz backdoor (CVE-2024-3094) on Forgejo. The infrastructure that powers Forgejo is not impacted by this vulnerability. Forgejo itself is also not affected, however if you run an OpenSSH server for Git over SSH you could be affected by this CVE.
---

On 29 March 2024 [the details were shared](https://www.openwall.com/lists/oss-security/2024/03/29/4) of a backdoor in the `xz` source code. This backdoor, according [to the latest reports](https://www.openwall.com/lists/oss-security/2024/03/30/36), is capable of executing arbitrary code on affected machines which can lead to a full compromise of that machine.

## Impact on Forgejo

Forgejo itself is not impacted by this backdoor, because it doesn't load the `liblzma` library that contained the backdoor, Forgejo does import the [xz](https://github.com/ulikunitz/xz) library but this has no relation to the `liblzma` library. Forgejo allows to use Git over SSH feature in two different ways.

### Builtin SSH server

The builtin SSH server is integrated into every Forgejo binary, it uses an [SSH server library](https://github.com/gliderlabs/ssh) to provide an SSH server. It is written in Go and doesn't load the `liblzma` library, therefore it is not affected.

Please read on if you're using Forgejo in a containerized environment such as Docker.

### OpenSSH server

If you run an OpenSSH server that you installed independently of Forgejo, you could be affected by the backdoor. It depends on your distribution and configuration: the vulnerable `xz` versions (`5.6.0`, `5.6.1`) could be in the package store and may have been installed. If you discover that your machine had it installed at any time, you should refer to your distribution's support or security advisories for more accurate information and to better understand the impact of this CVE on your installation.

If you deployed Forgejo using our official container images that also bundle an OpenSSH server, rest assured that these images **never contained a vulnerable version**. There is no need for action, because the images were based on Alpine Linux 3.19, which is not affected as per https://security.alpinelinux.org/vuln/CVE-2024-3094.

## Impact on infrastructure

The infrastructure that powers Forgejo consists of:

- Forgejo's own hardware used to create and sign the releases.
- Codeberg that hosts Forgejo's source code and the releases.

These machines all run Debian GNU/Linux Bookworm, a stable version of Debian. [The security advisory](https://lists.debian.org/debian-security-announce/2024/msg00057.html) of Debian for this CVE clarified that no Debian stable versions were impacted by this backdoor. Additionally, the [public detection script](https://www.openwall.com/lists/oss-security/2024/03/29/4/3) was run on the Forgejo machines and confirmed that the vulnerable `liblzma` library is not present on the machine.
